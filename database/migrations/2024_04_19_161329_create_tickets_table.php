<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('train');
            $table->date('datepicker');
            $table->string('name');
            $table->string('cnic');
            $table->string('addressPhone');
            $table->string('class');
            $table->string('coach');
            $table->string('type');
            $table->integer('seats');
            $table->string('from');
            $table->string('to');
            $table->decimal('charges', 8, 2);
            $table->date('ticketing_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
};
