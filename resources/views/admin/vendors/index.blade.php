@extends('admin.layouts.master')
@section('title', 'Vender')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Vender</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Vender
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th>Sr.No</th>
                                    <th>Vendor Name</th>
                                    <th>User Name</th>
                                    <th>Train Name</th>
                                    <th>Train class Name</th>
                                    <th>Train Coach Name</th>
                                    <th>Total Allowed Seats</th>
                                    <!-- <th class="not_include">Actions</th> -->
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!--Add Modal -->
                <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel17">Add Vender</h4>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <form class="form" id="add_form">
                                @csrf
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="first_name">First Name</label>
                                                <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="last_name">Last Name</label>
                                                <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="email">E-Mail</label>
                                                <input type="email" name="email" id="email" class="form-control" placeholder="E-Mail" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label for="profile_pic" class="form-label">Profile Photo</label>
                                                <input class="form-control" name="profile_pic" type="file" id="profile_pic">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="password">Password</label>
                                                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="password_confirmation">Confirm Password</label>
                                                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="role">Role</label>
                                                <select name="role" id="role" class="select2 form-select" data-placeholder="Select Role">
                                                    <option value=""></option>
                                                    @foreach ($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="vendor_group_id">Vendor Group</label>
                                                <select name="vendor_group_id" id="vendor_group_id" class="select2 form-select" data-placeholder="Select Vendor Group">
                                                    <option value=""></option>
                                                    @foreach ($vendor_groups as $vendor_group)
                                                    <option value="{{$vendor_group->id}}">{{$vendor_group->title}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="Train">Train</label>
                                                <select name="train_id" id="Train" class="select2 form-select" data-placeholder="Select Train">
                                                    <option value=""></option>
                                                    @foreach ($tains as $tain)
                                                    <option value="{{$tain->id}}">{{$tain->title}}</option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>
                                        <div class="add_payments" id="add_payment">
                                            <div data-repeater-list="charges">
                                                <div data-repeater-item>
                                                    <div class="row d-flex align-items-end">
                                                    <div class="col-3">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="Class">Class</label>
                                                            <select name="charges[][train_class_id]" id="class" class="select2 form-select" data-placeholder="Select class">
                                                                <option value=""></option>
                                                                @foreach ($classes as $class)
                                                                <option value="{{$class->id}}">{{$class->title}}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-3 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="coaches">Coaches</label>
                                                                <select name="charges[][train_coaches_id]" id="coaches" class="select2 form-select" data-placeholder="Select Coaches">
                                                                    <option value=""></option>
                                                                    @foreach ($coaches as $coach)
                                                                    <option value="{{$coach->id}}">{{$coach->coach_name_or_no}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-12">
                                                            <div class="mb-1">
                                                                <label class="form-label" for="total_allowed_seats">Total Allowed Seats</label>
                                                                <input type="number" name="charges[][total_allowed_seats]" id="total_allowed_seats" class="form-control" placeholder="Total Allowed Seats" min="1" required />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1 col-12">
                                                            <div class="mb-1">
                                                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                                    <i data-feather="x" class="me-25"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                        <i data-feather="plus" class="me-25"></i>
                                                        <span>Add New</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                    <button type="submit" class="btn btn-primary" id="save">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Vendor</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="first_name">First Name</label>
                                            <input type="text" name="first_name" id="edit_first_name" class="form-control" placeholder="First Name" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="last_name">Last Name</label>
                                            <input type="text" name="last_name" id="edit_last_name" class="form-control" placeholder="Last Name" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label class="form-label" for="email">E-Mail</label>
                                            <input type="email" name="email" id="edit_email" class="form-control" placeholder="E-Mail" required />
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="vendor_group_id">Vendor Group</label>
                                            <select name="vendor_group_id" id="edit_vendor_group_id" class="select2 form-select" data-placeholder="Select Group">
                                                <option value=""></option>
                                                @foreach ($vendor_groups as $vendor_group)
                                                <option value="{{$vendor_group->id}}">{{$vendor_group->title}}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                   
                                    <div class="col-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_Train">Train</label>
                                            <select name="train_id" id="edit_Train" class="select2 form-select" data-placeholder="Select Train">
                                                <option value=""></option>
                                                @foreach ($tains as $tains)
                                                <option value="{{$tains->id}}">{{$tains->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- Repeater Section -->
                                <div class="add_payments" id="edit_payment">
                                    <div data-repeater-list="charges">
                                        <!-- Repeater Item -->
                                        <div data-repeater-item>
                                            <div class="row d-flex align-items-end">
                                                <!-- <div class="col-md-4 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_coaches">Coaches</label>
                                                        <select name="charges[][train_coaches_id]" id="edit_coaches" class="select2 form-select" data-placeholder="Select Coaches">
                                                            <option value=""></option>
                                                            @foreach ($coaches as $coach)
                                                            <option value="{{$coach->id}}">{{$coach->coach_name_or_no}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_total_allowed_seats">Total Allowed Seats</label>
                                                        <input type="number" name="charges[][total_allowed_seats]" id="edit_total_allowed_seats" class="form-control" placeholder="Total Allowed Seats" min="1" max="15" required />
                                                    </div>
                                                </div> -->
                                                <!-- <div class="col-6">
                                                    <div class="mb-1">
                                                        <label class="form-label" for="edit_class">Class</label>
                                                        <select name="charges[][train_class_id]" id="edit_class" class="select2 form-select" data-placeholder="Select class">
                                                            <option value=""></option>
                                                            @foreach ($classes as $class)
                                                            <option value="{{$class->id}}">{{$class->title}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-1 col-12">
                                                    <div class="mb-1">
                                                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                                            <i data-feather="x" class="me-25"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                        <!-- /Repeater Item -->
                                    </div>
                                    <div class="row">
                                        <div class="col-12">
                                            <button class="btn btn-icon btn-primary" type="button" data-repeater-create>
                                                <i data-feather="plus" class="me-25"></i>
                                                <span>Add New</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- /Repeater Section -->
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>

@endsection
@section('scripts')
<script>
    var rowid;
    $(document).ready(function() {
        dataTable = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('vendors.index') }}",
            columns: [{
                    data: 'responsive_id'
                },
                {
                    data: 'id',
                    name: 'id',
                },
                {
                    data: 'vendor_name',
                    name: 'vendor_name',
                },
                {
                    data: 'first_name',
                    name: 'first_name',
                },
                {
                    data: 'title',
                    name: 'title',
                },
                {
                    data: 'train_classes_title',
                    name: 'train_classes_title',
                },
                {
                    data: 'train_classes_title',
                    name: 'train_classes_title',
                },
                {
                    data: ''
                },
            ],
            "columnDefs": [{
                    // For Responsive
                    className: 'control',
                    orderable: false,
                    searchable: false,
                    targets: 0
                },
                {
                    // Actions
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full, meta) {
                        return (
                            '<a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#edit-modal" class="item-edit" onclick=user_edit(' + full.user_id + ')>' +
                            feather.icons['edit'].toSvg({
                                class: 'font-medium-4'
                            }) +
                            '</a>'
                        );
                    }
                },
                {
                    "defaultContent": "-",
                    "targets": "_all"
                }
            ],
            "order": [
                [0, 'asc']
            ],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                        }, 50);
                    }
                },
                {
                    text: feather.icons['plus'].toSvg({
                        class: 'me-50 font-small-4'
                    }) + 'Add New',
                    className: 'create-new btn btn-primary',
                    attr: {
                        'data-bs-toggle': 'modal',
                        'data-bs-target': '#add_modal'
                    },
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                    }
                }
            ],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });
        $('div.head-label').html('<h6 class="mb-0">List of Vendors</h6>');

        // form repeater jquery
        var payment = $('#add_payment');
        // form repeater jquery
        var payment = $('#add_payment');
        $('.add_payments, .repeater-default').repeater({
            show: function() {
                $(this).slideDown();
                payment.find('select').next('.select2-container').remove();
                payment.find('select').select2({
                    placeholder: "Select Charges Type"
                });
                // Feather Icons
                if (feather) {
                    feather.replace({
                        width: 14,
                        height: 14
                    });
                }
            },
            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
        // Add New Charge button click event

        $("#add_form").submit(function(e) {
            e.preventDefault();

            // Get the number of records added
            var recordCount = $('#add_payment [data-repeater-item]').length;

            // Create FormData object
            var formData = new FormData(this);

            // Append record count to FormData
            var a = formData.append('record_count', recordCount);

            console.log(a);


            // Send AJAX request
            $.ajax({
                url: "{{ route('vendors.store') }}",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                responsive: true,
                success: function(response) {
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            });
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has occurred! Please contact the administrator.'
                        });
                    } else {
                        $('#add_form')[0].reset();
                        $("#add_modal").modal("hide");
                        dataTable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Vendors have been added successfully!'
                        });
                    }
                }
            });
        });


        $("#edit_form").submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{url('vendors')}}" + "/" + rowid,
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response) {
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#edit_form')[0].reset();
                        $("#edit_modal").modal("hide");
                        dataTable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Vendors has been Updated Successfully!'
                        })
                    }
                }
            });
        });
    });

    function user_edit(id) {
    rowid = id;
    $.ajax({
        url: "{{url('vendors')}}" + "/" + rowid + "/edit",
        type: "GET",
        success: function(response) {
            // Set basic vendor details
            $('#edit_first_name').val(response.vendor[0].first_name);
            $('#edit_last_name').val(response.vendor[0].last_name);
            $('#edit_email').val(response.vendor[0].email);
            $('#edit_vendor_group_id').val(response.vendor[0].vendor_group_id).select2();
            $('#edit_class').val(response.vendor[0].train_class_id).select2();  // Train Class ID
            $('#edit_Train').val(response.vendor[0].train_id).select2();

            // Update repeater section if needed
            var vendors = response.vendor;  // Assuming response.vendor contains the array of vendor records
            var repeaterList = $('#edit_payment [data-repeater-list="charges"]');
            repeaterList.empty();  // Clear previous items

            $.each(vendors, function(index, vendor) {
                var newItem = `
                <div data-repeater-item>
                    <div class="row d-flex align-items-end">
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="edit_coaches">Coaches</label>
                                <select name="charges[${index}][train_coaches_id]" class="select2 form-select" data-placeholder="Select Coaches">
                                    <option value=""></option>`;

                // Add options for coaches
                $.each(response.coaches, function(i, coach) {
                    newItem += `<option value="${coach.id}" ${coach.id == vendor.train_coaches_id ? 'selected' : ''}>${coach.coach_name_or_no}</option>`;
                });

                newItem += `
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="edit_class">Train Class</label>
                                <select name="charges[${index}][train_class_id]" class="select2 form-select" data-placeholder="Select Class">
                                    <option value=""></option>`;

                // Add options for train classes
                $.each(response.classes, function(i, trainClass) {
                    newItem += `<option value="${trainClass.id}" ${trainClass.id == vendor.train_class_id ? 'selected' : ''}>${trainClass.title}</option>`;
                });

                newItem += `
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-12">
                            <div class="mb-1">
                                <label class="form-label" for="edit_total_allowed_seats">Total Allowed Seats</label>
                                <input type="number" name="charges[${index}][total_allowed_seats]" class="form-control" placeholder="Total Allowed Seats" value="${vendor.total_allowed_seats}" min="1" required />
                            </div>
                        </div>
                        <div class="col-md-1 col-12">
                            <div class="mb-1">
                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                    <i data-feather="x" class="me-25"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <hr />
                </div>`;

                repeaterList.append(newItem);
            });

            $('#edit_modal').modal('show');
        }
    });
}


</script>
@endsection