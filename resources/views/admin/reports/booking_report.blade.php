@extends('admin.layouts.master')
@section('title', 'Booking Report')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Ticket-System</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/booking_report')}}">Reports</a>
                            </li>
                            <li class="breadcrumb-item active">Booking Report
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="basic-datatable">
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label for="fromdate">From</label>
                                        <input type="datetime-local" name="fromdate" id="fromdate" class="form-control" value="{{request('fromdate')}}">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="todate">To</label>
                                            <input type="datetime-local" name="todate" id="todate" class="form-control" value="{{request('todate')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="user">User</label>
                                        <select name="user[]" id="user" class="form-select select2" data-placeholder="Select User" aria-selected="" multiple>
                                            <label for="operation">Select User</label>
                                            @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->first_name."-".$user->last_name}}</option>
                                            @endforeach
                                        </select>
                                        <div class="button-container mt-1 mb-1">
                                            <button class="btn btn-sm btn-primary" type="button" onclick="selectAll('#user')">Select All</button>
                                            <button class="btn btn-sm btn-danger" type="button" onclick="deselectAll('#user')">Deselect All</button>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="train">Train</label>
                                        <select name="train" id="train" class="form-select select2" data-placeholder="Select Operation" aria-selected="">
                                            <option value="">Select Operation</option>
                                            @foreach($trains as $train)
                                            <option value="{{$train->id}}">{{$train->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="train_schedule">Train Schedule</label>
                                        <select name="train_schedule" id="train_schedule" class="form-select select2" data-placeholder="Select Schedule" aria-selected="" >
                                            <option value="">Select Schedule</option>

                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="train_class">Train Class</label>
                                        <select name="train_class" id="train_class" class="form-select select2" data-placeholder="Select Shift" aria-selected="" >
                                            <option value="">Select Train Class</option>
                                            @foreach($train_classes as $train_class)
                                            <option value="{{$train_class->id}}">{{$train_class->title}}</option>
                                            @endforeach

                                        </select>
                                    </div> 
                                     <div class="col-md-6 col-12 mt-2">
                                        <label for="train_coach">Train Coach</label>
                                        <select name="train_coach" id="train_coach" class="form-select select2" data-placeholder="Select Shift" aria-selected="" >
                                            <option value="">Select Train Coach</option>

                                        </select>
                                    </div>                                 
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{url('/booking_report')}}" type="button" class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                    <div class="col-8">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card">
                        <table class="table" id="dataTable">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Ticket_Number</th>
                                    <th>Customer Name</th>
                                    <th>Customer CNIC</th>
                                    <th>Customer Ph#</th>
                                    <th>Seating Person Gender</th>
                                    <!-- <th>Seat_#</th> -->
                                    <th>Ticket Genius</th>
                                    <th>Train</th>
                                    <th>Train Schedule</th>
                                    <th>Train Class</th>
                                    <th>Train Coach</th>
                                    <th>Charges</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($bookings)
                            @foreach($bookings as $key => $booking)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$booking->ticket_number}}</td>
                                    <td>{{$booking->customer_name}}</td>
                                    <td>{{$booking->customer_cnic}}</td>
                                    <td>{{$booking->customer_phone_number}}</td>
                                    <td>{{$booking->seating_person_gender}}</td>
                                    <!-- <td>{{$booking->seat_number}}</td> -->
                                    <td>{{$booking->full_name}}</td>
                                    <td>{{$booking->train_name}}</td>
                                    <td>{{$booking->schedule_day}}</td>
                                    <td>{{$booking->class_name}}</td>
                                    <td>{{$booking->coach_name}}</td>
                                    <td>{{$booking->total_charges}}</td>


                                </tr>
                            @endforeach
                            @endif
                            <tbody>
                            @if($bookings)
                                <tfoot>
                                    <tr>
                                        <td>-</td>
                                        <td>Grand Total</td>
                                         <td>-</td> 
                                         <td>{{$bookings->sum('total_charges')}}</td> 
                                         <td>-</td> 
                                         <td>-</td> 
                                         <!-- <td>-</td> -->
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        
                                    </tr>
                                </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
            </div>

        </section>
    </div>
</section>

@endsection
@section('scripts')
<script>
    var rowid;
    $(document).ready(function() {
        dataTable = $('#dataTable').DataTable({
            ordering: true,
            "columnDefs": [{
                // For Responsive
                className: 'control',
                orderable: false,
                targets: 0
            }],
            dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
            displayLength: 10,
            lengthMenu: [10, 25, 50, 75, 100],
            buttons: [{
                extend: 'collection',
                className: 'btn btn-outline-secondary dropdown-toggle me-2',
                text: feather.icons['share'].toSvg({
                    class: 'font-small-4 me-50'
                }) + 'Export',

                buttons: [{

                        extend: 'print',
                        text: feather.icons['printer'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Print',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'csv',
                        text: feather.icons['file-text'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Csv',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'excel',
                        text: feather.icons['file'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Excel',
                        className: 'dropdown-item',
                        footer: true,
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    },
                    {
                        extend: 'pdf',
                        text: feather.icons['clipboard'].toSvg({
                            class: 'font-smaller me-50'
                        }) + 'Pdf',
                        className: 'dropdown-item',
                        orientation: 'landscape',
                        footer: true,
                        pageSize: 'A3',
                        customize: function(doc) {
                            //Remove the title created by datatTables
                            doc.content.splice(0, 1);
                            //Create a date string that we use in the footer. Format is dd-mm-yyyy
                            var now = new Date();
                            var jsDate = now.getDate() + "-" + (now.getMonth() + 1) + "-" + now.getFullYear();
                            doc.pageMargins = [20, 60, 20, 30];
                            // Set the font size fot the entire document
                            doc.defaultStyle.fontSize = 12;
                            // Set the fontsize for the table header
                            doc.styles.tableHeader.fontSize = 10;
                            // Create a header
                            doc["header"] = function() {
                                return {
                                    columns: [{
                                        alignment: "center",
                                        italics: true,
                                        text: "Vehicle Report",
                                        fontSize: 18,
                                        margin: [10, 0]
                                    }, ],
                                    margin: 20
                                };
                            };
                            doc["footer"] = function(page, pages) {
                                return {
                                    columns: [{
                                            alignment: "left",
                                            text: ["Created on: ", {
                                                text: jsDate.toString()
                                            }]
                                        },
                                        {
                                            alignment: "right",
                                            text: [
                                                "page ",
                                                {
                                                    text: page.toString()
                                                },
                                                " of ",
                                                {
                                                    text: pages.toString()
                                                }
                                            ]
                                        }
                                    ],
                                    margin: 20
                                };
                            };
                            var objLayout = {};
                            objLayout["hLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["vLineWidth"] = function(i) {
                                return 0.5;
                            };
                            objLayout["hLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["vLineColor"] = function(i) {
                                return "#aaa";
                            };
                            objLayout["paddingLeft"] = function(i) {
                                return 4;
                            };
                            objLayout["paddingRight"] = function(i) {
                                return 4;
                            };
                            doc.content[0].layout = objLayout;
                        },
                        // action: newexportaction,
                        exportOptions: {
                            columns: ':not(.not_include)',
                            // tfoot:true,
                        }
                    },
                    {
                        extend: 'copy',
                        text: feather.icons['copy'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Copy',
                        className: 'dropdown-item',
                        exportOptions: {
                            columns: ':not(.not_include)'
                        }
                    }
                ],
                init: function(api, node, config) {
                    $(node).removeClass('btn-secondary');
                    $(node).parent().removeClass('btn-group');
                    setTimeout(function() {
                        $(node).closest('.dt-buttons').removeClass('btn-group').addClass('d-inline-flex');
                    }, 50);
                }
            }],
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.childRowImmediate,
                    type: 'column',
                }
            },
            language: {
                paginate: {
                    // remove previous & next text from pagination
                    previous: '&nbsp;',
                    next: '&nbsp;'
                }
            }
        });

        $('div.head-label').html('<h6 class="mb-0">Vehicle Report</h6>');
        $('.app').attr('id', 'app-export');

    });
 $('#train').change(function() {
                var trainId = $(this).val();
                $('#trainDate').empty();
                $('#trainDate').prop('disabled', false);

                $.ajax({
                    url: '{{ route('get.train.dates', ['trainId' => ':trainId']) }}'.replace(
                        ':trainId', trainId),
                    type: 'GET',
                    success: function(response) {
                        console.log(response.data)
                        var dates = response.dates;
                        $('#train_schedule').append('<option value="">Select Date</option>');
                        Object.keys(dates).forEach(function(key) {
                            $('#train_schedule').append('<option value="' + key + '">' +
                                dates[key] + '</option>');
                        });
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
            });
            $('#train_class').change(function() {
                var selectedClassId = $(this).val();
                $('#train_coach').empty();
                $('#train_coach').prop('disabled', false);

                $.ajax({
                    url: "{{ url('get-coaches-by-class') }}" + "/" + selectedClassId,
                    type: 'GET',
                    success: function(response) {
                        var coaches = response.coaches;
                        console.log(response);
                        $('#train_coach').empty();
                        $('#train_coach').append('<option value="">Select Coach</option>');
                        coaches.forEach(function(coach) {
                            $('#train_coach').append('<option value="' + coach.id + '">' +
                                coach.coach_name_or_no + '</option>');
                        });
                    },
                    error: function(error) {
                        console.log(error);
                    }
                });
  });

</script>
@endsection