@extends('admin.layouts.master')
@section('title', 'Coaches')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Coaches</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Coaches</a>
                                </li>
                                <li class="breadcrumb-item active">Coaches
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Coach Name or No</th>
                                        <th>Number of Seats</th>
                                        <th>Train</th>
                                        <th>Train Class</th>
                                        <th>Adult Charges</th>
                                        <th>Child Charges</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Coaches</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="coach_name_or_no">Coach Name Or No</label>
                                            <input type="text" class="form-control" placeholder="coach_name_or_no"
                                                name="coach_name_or_no" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="number_of_seats">Number of Seats</label>
                                            <input type="text" class="form-control" placeholder="number_of_seats"
                                                name="number_of_seats" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="train_id">Train</label>
                                            <select name="train_id" id="train_id" class="select2 form-select"
                                                data-placeholder="Select Train">
                                                @foreach ($trains as $train)
                                                    <option
                                                        value="{{ $train->id }}"{{ $train->id == request('train_id') ? 'selected' : '' }}>
                                                        {{ $train->title }}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" class="form-control" placeholder="train_id"
                                                name="train_id" required /> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="train_class_id">Train Class</label>
                                            <select name="train_class_id" id="train_class_id" class="select2 form-select"
                                                data-placeholder="Select Classes">
                                                @foreach ($classes as $class)
                                                    <option
                                                        value="{{ $class->id }}"{{ $class->id == request('train_class_id') ? 'selected' : '' }}>
                                                        {{ $class->title }}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" class="form-control" placeholder="train_class_id"
                                                name="train_class_id" required /> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="adult_charges">Adult_charges</label>
                                            <input type="text" class="form-control" placeholder="adult_charges"
                                                name="adult_charges" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="child_charges">Child_charges</label>
                                            <input type="text" class="form-control" placeholder="child_charges"
                                                name="child_charges" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->

            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Coaches</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_coach_name_or_no">Coach Name or No</label>
                                            <input type="text" class="form-control" id="edit_coach_name_or_no"
                                                placeholder="coach_name_or_no" name="coach_name_or_no" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_number_of_seats">Number of Seats</label>
                                            <input type="text" class="form-control" id="edit_number_of_seats"
                                                placeholder="number_of_seats" name="number_of_seats" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_train_id">Train</label>
                                            <select name="train_id" id="edit_train_id" class="select2 form-select"
                                                data-placeholder="Select Train">
                                                @foreach ($trains as $train)
                                                    <option
                                                        value="{{ $train->id }}"{{ $train->id == request('train_id') ? 'selected' : '' }}>
                                                        {{ $train->title }}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" class="form-control" id="edit_train_id"
                                                placeholder="train_id" name="train_id" required /> --}}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_train_class_id">Train Class</label>
                                            <select name="train_class_id" id="edit_train_class_id"
                                                class="select2 form-select" data-placeholder="Select Class">
                                                @foreach ($classes as $class)
                                                    <option
                                                        value="{{ $class->id }}"{{ $class->id == request('train_class_id') ? 'selected' : '' }}>
                                                        {{ $class->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_adult_charges">Adult Charges</label>
                                            <input type="text" class="form-control" id="edit_adult_charges"
                                                placeholder="adult_charges" name="adult_charges" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_child_charges">Child Charges</label>
                                            <input type="text" class="form-control" id="edit_child_charges"
                                                placeholder="child_charges" name="child_charges" required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('train_coaches.index') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        data: 'coach_name_or_no',
                        name: 'coach_name_or_no'
                    },
                    {
                        data: 'number_of_seats',
                        name: 'number_of_seats'
                    },
                    {
                        data: 'train',
                        name: 'train.title'
                    },
                    {
                        data: 'train_classes',
                        name: 'classes.title'
                    },
                    {
                        data: 'adult_charges',
                        name: 'adult_charges'
                    },
                    {
                        data: 'child_charges',
                        name: 'child_charges'
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [
                    // {
                    // extend: 'collection',
                    // className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    // text: feather.icons['share'].toSvg({
                    //     class: 'font-small-4 me-50'
                    // }) + 'Export',
                    // buttons: [
                    //     {
                    //         extend: 'print',
                    //         text: feather.icons['printer'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Print',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'csv',
                    //         text: feather.icons['file-text'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Csv',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'excel',
                    //         text: feather.icons['file'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Excel',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'pdf',
                    //         text: feather.icons['clipboard'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Pdf',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'copy',
                    //         text: feather.icons['copy'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Copy',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     }
                    // ],
                    // init: function(api, node, config) {
                    //     $(node).removeClass('btn-secondary');
                    //     $(node).parent().removeClass('btn-group');
                    //     setTimeout(function() {
                    //         $(node).closest('.dt-buttons').removeClass('btn-group')
                    //             .addClass('d-inline-flex');
                    //     }, 50);
                    // }
                    // },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Coaches</h6>');

            // add record
            $("#add_form").submit(function(e) {
                var train_class = $('#train_class_id option:selected').text().trim();
                var formData = new FormData(this);
                formData.append('train_class', train_class);
                e.preventDefault();
                $.ajax({
                    url: "{{ route('train_coaches.store') }}",
                    type: "post",
                    data: formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        $.unblockUI();
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Coach has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function(e) {
                var train_class = $('#edit_train_class_id option:selected').text().trim();
                var formData = new FormData(this);
                formData.append('train_class',train_class);
                e.preventDefault();
                $.ajax({
                    url: "{{ url('train_coaches') }}" + "/" + rowid,
                    type: "post",
                    data:formData,
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Coaches has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('train_coaches') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $("#edit_coach_name_or_no").val(response.coach_name_or_no);
                    $("#edit_number_of_seats").val(response.number_of_seats);
                    $("#edit_train_id").val(response.train_id);
                    $("#edit_train_class_id").val(response.train_class_id);
                    $("#edit_adult_charges").val(response.adult_charges);
                    $("#edit_child_charges").val(response.child_charges);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "train_coaches/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Coach has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
