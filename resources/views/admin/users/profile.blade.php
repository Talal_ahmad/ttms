@extends('admin.layouts.master')
@section('title', 'User Profile')

@section('content')
<section class="content-wrapper container-xxl p-0">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Profile</h2>
                    {{-- <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Users
                            </li>
                        </ol>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
    <div class="content-header row"></div>
    <div class="content-body">
        <section id="page-account-settings">
            <div class="row">
                <!-- left menu section -->
                <div class="col-md-3 mb-2 mb-md-0">
                    <ul class="nav nav-pills flex-column nav-left">
                        <!-- general -->
                        <li class="nav-item">
                            <a class="nav-link {{request('type') == 'general' ? 'active' : ''}}" id="account-pill-general" href="{{url('profile')}}?type=general">
                                <i data-feather="user" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">General</span>
                            </a>
                        </li>
                        <!-- change password -->
                        <li class="nav-item">
                            <a class="nav-link {{request('type') == 'changePassowrd' ? 'active' : ''}}" id="account-pill-password" href="{{url('profile')}}?type=changePassowrd">
                                <i data-feather="lock" class="font-medium-3 me-1"></i>
                                <span class="fw-bold">Change Password</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--/ left menu section -->

                <!-- right content section -->
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <div class="tab-content">
                                <!-- general tab -->
                                <div role="tabpanel" class="tab-pane {{request('type') == 'general' ? 'active' : ''}}" id="account-pill-general" aria-labelledby="account-pill-general">
                                    <!-- header section -->
                                    <div class="d-flex mb-1">
                                        <a href="#" class="me-25">
                                            <img src="{{ !empty(Auth::user()->profile_photo_path) ? asset('profile_pics/'.Auth::user()->profile_photo_path) : asset('app-assets/images/portrait/small/avatar-s-11.jpg')}}" id="account-upload-img" class="rounded me-50" alt="profile image" height="80" width="80" />
                                        </a>
                                        <!-- upload and reset button -->
                                        <!-- form -->
                                    <form action="{{url('updateUserProfile')}}" method="POST" class="validate-form mt-2" enctype="multipart/form-data">
                                        @csrf
                                        <div class="mt-75 ms-1">
                                            <label for="account-upload" class="btn btn-sm btn-primary mb-75 me-75">Upload</label>
                                            <input type="file" name="profile_pic" id="account-upload" hidden accept="image/*" />
                                            <button class="btn btn-sm btn-outline-secondary mb-75">Reset</button>
                                            <p>Allowed JPG, GIF or PNG. Max size of 800kB</p>
                                        </div>
                                        <!--/ upload and reset button -->
                                    </div>
                                    <!--/ header section -->
                                        @if(Session::has('success_message'))
                                            <div class="alert alert-success p-1">
                                                {{Session::get('success_message')}}
                                            </div>
                                        @endif
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-username">First Name</label>
                                                    <input type="text" class="form-control" id="account-firstName" name="firstName" placeholder="First Name" value="{{Auth::user()->first_name}}" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-lastName">Last Name</label>
                                                    <input type="text" class="form-control" id="account-lastName" name="lastName" placeholder="Last Name" value="{{Auth::user()->last_name}}" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-e-mail">E-mail</label>
                                                    <input type="email" class="form-control" id="account-e-mail" name="email" placeholder="Email" value="{{Auth::user()->email}}" />
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-company">Company</label>
                                                    <input type="text" class="form-control" id="account-company" name="company" placeholder="Company name" value="Crystal Technologies" />
                                                </div>
                                            </div>
                                            {{-- <div class="col-12 mt-75">
                                                <div class="alert alert-warning mb-50" role="alert">
                                                    <h4 class="alert-heading">Your email is not confirmed. Please check your inbox.</h4>
                                                    <div class="alert-body">
                                                        <a href="javascript: void(0);" class="alert-link">Resend confirmation</a>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary mt-2 me-1">Save changes</button>
                                                <button type="reset" class="btn btn-outline-secondary mt-2">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!--/ form -->
                                </div>
                                <!--/ general tab -->

                                <!-- change password -->
                                <div role="tabpanel" class="tab-pane {{request('type') == 'changePassowrd' ? 'active' : ''}}" id="account-pill-password" aria-labelledby="account-pill-password">
                                    @if(Session::has('success_message'))
                                    <div class="alert alert-success p-1">
                                        {{Session::get('success_message')}}
                                    </div>
                                    @endif
                                    @if(Session::has('error_message'))
                                    <div class="alert alert-danger p-1">
                                        {{Session::get('error_message')}}
                                    </div>
                                    @endif
                                    <!-- form -->
                                    <form action="{{url('updateUserProfile')}}?type=changePassowrd" method="POST" class="validate-form">
                                        @csrf
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-old-password">Old Password</label>
                                                    <div class="input-group form-password-toggle input-group-merge">
                                                        <input type="password" class="form-control @error('old_password') is-invalid @enderror" id="account-old-password" name="old_password" placeholder="Old Password" required/>
                                                        <div class="input-group-text cursor-pointer">
                                                            <i data-feather="eye"></i>
                                                        </div>
                                                    </div>
                                                    @error('old_password')
                                                        <div class="alert alert-danger">{{$message}}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-new-password">New Password</label>
                                                    <div class="input-group form-password-toggle input-group-merge">
                                                        <input type="password" id="account-new-password" name="password" class="form-control" placeholder="New Password" required/>
                                                        <div class="input-group-text cursor-pointer">
                                                            <i data-feather="eye"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                                <div class="mb-1">
                                                    <label class="form-label" for="account-retype-new-password">Retype New Password</label>
                                                    <div class="input-group form-password-toggle input-group-merge">
                                                        <input type="password" class="form-control" id="account-retype-new-password" name="password_confirmation" placeholder="New Password" required/>
                                                        <div class="input-group-text cursor-pointer"><i data-feather="eye"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary me-1 mt-1">Save changes</button>
                                                <button type="reset" class="btn btn-outline-secondary mt-1">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!--/ form -->
                                </div>
                                <!--/ change password -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ right content section -->
            </div>
        </section>
    </div>
</section>

@endsection