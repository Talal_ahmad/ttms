@extends('admin.layouts.posmaster')

@section('title', 'POS')
@section('content')
    <style>
        input {
            font-family: 'Roboto', sans-serif;
            display: block;
            border: none;
            border-radius: 0.25rem;
            border: 1px solid transparent;
            line-height: 1.5rem;
            padding: 0;
            font-size: 1rem;
            color: #607D8B;
            width: 100%;
            margin-top: 0.5rem;
        }

        input:focus {
            outline: none;
        }

        .header-navbar {
            position: fixed !important;
            top: 0;
            left: 0;
            width: 100%;
            z-index: 1000;
        }

        .content-body {
            margin-top: 0px;
            /* Adjust this value to match the navbar's height */
        }

        #ui-datepicker-div {
            display: none;
            background-color: #fff;
            box-shadow: 0 0.125rem 0.5rem rgba(0, 0, 0, 0.1);
            margin-top: 0.25rem;
            border-radius: 0.5rem;
            padding: 0.5rem;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .ui-datepicker-calendar thead th {
            padding: 0.25rem 0;
            text-align: center;
            font-size: 0.75rem;
            font-weight: 400;
            color: #78909C;
        }

        .ui-datepicker-calendar tbody td {
            width: 2.5rem;
            text-align: center;
            padding: 0;
        }

        .ui-datepicker-calendar tbody td a {
            display: block;
            border-radius: 0.25rem;
            line-height: 2rem;
            transition: 0.3s all;
            color: #546E7A;
            font-size: 0.875rem;
            text-decoration: none;
        }

        .ui-datepicker-calendar tbody td a:hover {
            background-color: #E0F2F1;
        }

        .ui-datepicker-calendar tbody td a.ui-state-active {
            background-color: #009688;
            color: white;
        }

        .ui-datepicker-header a.ui-corner-all {
            cursor: pointer;
            position: absolute;
            top: 0;
            width: 2rem;
            height: 2rem;
            margin: 0.5rem;
            border-radius: 0.25rem;
            transition: 0.3s all;
        }

        .ui-datepicker-header a.ui-corner-all:hover {
            background-color: #ECEFF1;
        }

        .ui-datepicker-header a.ui-datepicker-prev {
            left: 0;
            background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMyIgdmlld0JveD0iMCAwIDEzIDEzIj48cGF0aCBmaWxsPSIjNDI0NzcwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik03LjI4OCA2LjI5NkwzLjIwMiAyLjIxYS43MS43MSAwIDAgMSAuMDA3LS45OTljLjI4LS4yOC43MjUtLjI4Ljk5OS0uMDA3TDguODAzIDUuOGEuNjk1LjY5NSAwIDAgMSAuMjAyLjQ5Ni42OTUuNjk1IDAgMCAxLS4yMDIuNDk3bC00LjU5NSA0LjU5NWEuNzA0LjcwNCAwIDAgMS0xLS4wMDcuNzEuNzEgMCAwIDEtLjAwNi0uOTk5bDQuMDg2LTQuMDg2eiIvPjwvc3ZnPg==");
            background-repeat: no-repeat;
            background-size: 0.5rem;
            background-position: 50%;
            transform: rotate(180deg);
        }

        .ui-datepicker-header a.ui-datepicker-next {
            right: 0;
            background: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIxMyIgaGVpZ2h0PSIxMyIgdmlld0JveD0iMCAwIDEzIDEzIj48cGF0aCBmaWxsPSIjNDI0NzcwIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik03LjI4OCA2LjI5NkwzLjIwMiAyLjIxYS43MS43MSAwIDAgMSAuMDA3LS45OTljLjI4LS4yOC43MjUtLjI4Ljk5OS0uMDA3TDguODAzIDUuOGEuNjk1LjY5NSAwIDAgMSAuMjAyLjQ5Ni42OTUuNjk1IDAgMCAxLS4yMDIuNDk3bC00LjU5NSA0LjU5NWEuNzA0LjcwNCAwIDAgMS0xLS4wMDcuNzEuNzEgMCAwIDEtLjAwNi0uOTk5bDQuMDg2LTQuMDg2eiIvPjwvc3ZnPg==');
            background-repeat: no-repeat;
            background-size: 10px;
            background-position: 50%;
        }

        .ui-datepicker-header a>span {
            display: none;
        }

        .ui-datepicker-title {
            text-align: center;
            line-height: 2rem;
            margin-bottom: 0.25rem;
            font-size: 0.875rem;
            font-weight: 500;
            padding-bottom: 0.25rem;
        }

        .ui-datepicker-week-col {
            color: #78909C;
            font-weight: 400;
            font-size: 0.75rem;
        }

        .btn-success {
            border-color: #02876E !important;
            background-color: #02876E !important;
            color: #fff !important;
        }

        .border-custom-color {
            border-color: rgba(114, 202, 138, 1) !important;
        }

        .custom_bg_green {
            background-color: rgba(237, 245, 244, 1) !important;
        }

        .custom_rounded {
            border-radius: 28px !important;
        }

        .theme_color_green {
            color: rgba(2, 135, 110, 1);
        }

        .form-check-input:checked {
            background-color: #02876E !important;
            border-color: #02876E !important
        }

        .loader-body {
            margin: 0;
            height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
            background: linear-gradient(#666, #333);
            /* position: absolute; */
            z-index: 10000;
            width: 100%;
        }

        .loader {
            width: 8em;
            height: 10em;
            font-size: 20px;
        }

        .train {
            width: 6em;
            height: 6em;
            background:
                radial-gradient(circle at 20% 80%, currentColor 0.6em, transparent 0.6em),
                radial-gradient(circle at 80% 80%, currentColor 0.6em, transparent 0.6em),
                #bbb;
            border-radius: 1em;
            position: relative;
            left: 1em;
            color: #444;
            animation: train-animate 1.5s infinite ease-in-out;
        }

        @keyframes train-animate {

            0%,
            100% {
                transform: rotate(0deg);
            }

            25%,
            75% {
                transform: rotate(0.5deg);
            }

            50% {
                transform: rotate(-0.5deg);
            }
        }

        .train::before {
            content: '';
            position: absolute;
            width: 80%;
            height: 2.3em;
            background-color: currentColor;
            border-radius: 0.4em;
            top: 1.2em;
            left: 10%;
        }

        .train::after {
            content: '';
            position: absolute;
            width: 25%;
            height: 0.4em;
            background-color: currentColor;
            border-radius: 0.3em;
            top: 0.4em;
            left: calc((100% - 25%) / 2);
        }

        .track {
            width: 8em;
            position: relative;
        }

        .track::before,
        .track::after {
            content: '';
            position: absolute;
            width: 0.3em;
            height: 4em;
            background-color: #bbb;
            border-radius: 0.4em;
            transform-origin: bottom;
        }

        .track::before {
            left: 0;
            transform: skewX(-27deg);
        }

        .track::after {
            right: 0;
            transform: skewX(27deg);
        }

        .track span {
            width: inherit;
            height: 0.3em;
            background-color: #bbb;
            position: absolute;
            top: 4em;
            animation: track-animate 1s linear infinite;
        }

        .track span:nth-child(2) {
            animation-delay: -0.33s;
        }

        .track span:nth-child(3) {
            animation-delay: -0.66s;
        }

        @keyframes track-animate {
            0% {
                transform: translateY(-0.5em) scaleX(0.9);
                filter: opacity(0);
            }

            10%,
            90% {
                filter: opacity(1);
            }

            100% {
                transform: translateY(-4em) scaleX(0.5);
                filter: opacity(0);
            }
        }
    </style>
    <section class="content-wrapper p-0">
        {{-- <div class="content-header row"></div> --}}
        <div class="content-body">
            <nav class="header-navbar align-items-center navbar-light navbar-shadow position-static">
                <div class="navbar-container d-flex content">
                    <div class="bookmark-wrapper d-flex align-items-center">
                        {{-- <ul class="nav navbar-nav d-xl-none">
                        <li class="nav-item">
                            <a class="nav-link menu-toggle" href="#"><i class="ficon" data-feather="menu"></i>nadhbamh</a>
                        </li>
                    </ul> --}}
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link menu-toggle" href="#"><img src="image1.png" alt=""
                                        width="100%" height="40px"></a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link menu-toggle" href="#">
                                    <h2 class="mt-1 text-success ">Pakistan Railways</h2>
                                </a>
                            </li>
                        </ul>
                        {{-- <ul class="nav navbar-nav bookmark-icons">
                            <li class="nav-item d-block">
                                <a class="nav-link nav-link-style"><i class="ficon mt-1" data-feather="moon"></i></a>
                            </li>
                        </ul> --}}
                    </div>
                    @if (auth()->user()->is_vendor == 1)
                        <div class="ms-auto me-3 my-auto">
                            <a href="{{ asset('booking_report') }}">
                                <button class="btn btn-success ">Booking Details</button>
                            </a>
                        </div>
                    @endif
                    <ul class="nav navbar-nav align-items-center my-auto {{ auth()->user()->is_vendor == 0 ? 'ms-auto' : '' }}">
                        <li class="nav-item dropdown dropdown-user">
                            <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="#"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="user-nav d-sm-flex d-none"><span
                                        class="user-name fw-bolder">{{ Auth::check() ? Auth::user()->first_name . ' ' . Auth::user()->last_name : 'User' }}</span><span
                                        class="user-status">{{ !empty(Auth::user()->getRoleNames()) ? Auth::user()->getRoleNames()[0] : 'Guest' }}</span>
                                </div>
                                <span class="avatar"><img class="round"
                                        src="{{ !empty(Auth::user()->profile_photo_path) ? asset('profile_pics/' . Auth::user()->profile_photo_path) : asset('app-assets/images/portrait/small/avatar-s-11.jpg') }}"
                                        alt="avatar" height="40" width="40" /><span
                                        class="avatar-status-online"></span></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="dropdown-user">
                                <a class="dropdown-item" href="{{ url('profile') }}?type=general"><i class="me-50"
                                        data-feather="user"></i> Profile</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                                    <i class="me-50" data-feather="power"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <section>
                <br><br><br><br>
                <br>
                <div class=" container-fluid">
                    <form id="ticketForm" action="{{ url('save-ticket') }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-xl-6 navbar-shadow p-0">
                                <div class="mx-1 navbar-light p-2 rounded-3">
                                    <h3 class="mt-0">Book Ticket</h3>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="border d-flex px-2">
                                                <span style="margin: 11px"><img src="fluent-mdl2_bus-solid.png"
                                                        alt=""></span>
                                                <select class="form-control border-0" aria-label="Default select example"
                                                    id="train" name="train">
                                                    <option value="">Select Train</option>
                                                    @foreach ($trains as $train)
                                                        <option value="{{ $train->id }}">{{ $train->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="border d-flex px-2">
                                                <span class="m-0 p-0"><img src="trailing-icon.png" alt=""></span>
                                                <select class="form-control" id="trainDate" name="trainDate"
                                                    disabled="true">
                                                    <option value="">Select Date</option>
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row seat-cards-container" style="background-image: url(back.png)">

                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-xl-6 navbar-shadow p-0">
                                <div class="mx-1 navbar-light p-2 rounded-3">
                                    <div class="d-flex justify-content-between align-items-center ">
                                        <div>
                                            <h3 class="mt-1 mb-1">Customer Detail</h3>
                                        </div>
                                        <div id="charges_child_adult" class="d-none">
                                            <span><span class=" fw-bold ">Adult Charges:</span> PKR <b
                                                    id="adult_charges"></b>/-</span><br>
                                            <span><span class=" fw-bold ">Child Charges:</span> PKR <b
                                                    id="child_charges"></b>/-</span>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-6">
                                            <label for="name" class="form-label">Name:</label>
                                            <input type="text" class="form-control mt-0" id="name" name="name"
                                                placeholder="Enter Your Name" required>
                                        </div>
                                        <div class="col-6">
                                            <label for="cnic" class="form-label">CNIC:</label>
                                            <input type="text" class="form-control mt-0"
                                                data-inputmask="'mask': '99999-9999999-9'" placeholder="XXXXX-XXXXXXX-X"
                                                id="cnic" name="cnic" required>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-12">
                                            <label for="addressPhone" class="form-label">Address/Phone#:</label>
                                            <input type="text" class="form-control mt-0" id="addressPhone"
                                                name="addressPhone" placeholder="Please Enter Your Address/Phone No."
                                                required>
                                        </div>
                                    </div>
                                    <input type="hidden" name="charges" id="singleCharges" value="">
                                    <div class="row mb-1">
                                        <div class="col-6">
                                            <label for="class" class="form-label">Class:</label>
                                            <select class="form-select" id="class" name="class" disabled="true">
                                                <option value="">Select Class</option>
                                                <option value="1">Test Class</option>
                                            </select>
                                        </div>
                                        <div class="col-6">
                                            <label for="coach" class="form-label">Coach:</label>
                                            <select class="form-select" id="coach" name="coach" disabled="true">
                                                <option value="">Select Coach</option>
                                                <option value="1">Test Coach</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-6">
                                            <label for="type" class="form-label">Type:</label>
                                            <select class="form-select" id="type" name="type" disabled="true">
                                                <option value="">Select</option>
                                                <option value="1">Adult</option>
                                                <option value="2">Child</option>
                                            </select>
                                        </div>
                                        {{-- <div class="col-3">
                                            <label for="checkbox" class="form-label">No. Tickets</label>
                                            <input class="form-control mt-0" type="number" name="seats"
                                                id="seats">
                                        </div> --}}
                                        <div class="col-2">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="seating_person_gender" id="flexRadioDefault1">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Family
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="seating_person_gender" id="flexRadioDefault2" checked>
                                                <label class="form-check-label" for="flexRadioDefault2">
                                                    Female
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio"
                                                    name="seating_person_gender" id="flexRadioDefault3" checked>
                                                <label class="form-check-label" for="flexRadioDefault3">
                                                    Male
                                                </label>
                                            </div>
                                        </div>
                                        {{-- <div class="col-2">
                                            <label for="seatnum" class="form-label">Seats:</label>
                                            <input type="text" class="form-control mt-0 seatnum" id="seatnum"
                                                name="seatnum" required>
                                        </div> --}}
                                        <div class="col-4">
                                            <button type="button" class="btn btn-success mt-2 px-4 rounded-pill"
                                                id="seating_plan" style="padding:11px" data-bs-toggle="modal"
                                                data-bs-target="#exampleModal" disabled="true">
                                                Seating Plan
                                            </button>
                                        </div>
                                    </div>
                                    <div class="modal fade " id="exampleModal" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-fullscreen ">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h1 class="modal-title fs-3 fw-bolder" id="exampleModalLabel">Coach
                                                        Sitting Plan
                                                    </h1>
                                                    <button type="button" class="btn-close reject_ajax_seat_plan" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row row-cols-2 mt-1">
                                                        <div class=" custom_rounded custom_bg_green pb-4 position-relative ">
                                                            <div
                                                                class="w-50 mb-3 mx-auto border border-custom-color custom_rounded box-shadow-3 text-center">
                                                                <h1 class="py-1 m-0 theme_color_green"
                                                                    id="seating_plan_coach_no">
                                                                    Coach(01) # zam223</h1>
                                                            </div>
                                                            <div class="loader-body rounded-3">
                                                                <div class="loader">
                                                                    <div class="train"></div>
                                                                    <div class="track">
                                                                        <span></span>
                                                                        <span></span>
                                                                        <span></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row row-cols-5 gap-1 justify-content-center"
                                                            id="seating_plan_seats">
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class=" ms-5 ps-5">
                                                                <div class="ms-5 ps-5 mb-2 d-flex align-items-center ">
                                                                    <div class="bg-danger w-25 rounded-pill box-shadow-3">
                                                                        <br><br>
                                                                    </div>
                                                                    <div class=" ms-1 fs-3 fw-bold">Allocated Seats</div>
                                                                </div>
                                                                <div class="ms-5 ps-5 d-flex align-items-center mb-2">
                                                                    <div class="bg-success w-25 rounded-pill box-shadow-3">
                                                                        <br><br>
                                                                    </div>
                                                                    <div class=" ms-1 fs-3 fw-bold">Selected Seats</div>
                                                                </div>
                                                            </div>
                                                            <div class=" text-center ">
                                                                <img src="{{ asset('app-assets/images/elements/image 5.png') }}"
                                                                    alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary reject_ajax_seat_plan"
                                                        data-bs-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="selected_seats[]" id="selected_seats" value="">
                                    <input type="hidden" name="send_ajax_seat_plan" id="send_ajax_seat_plan" value="send">
                                    <div class="row mb-2">
                                        <div class="col-6">
                                            <label for="total_charges" class="form-label">Charges:</label>
                                            <input type="number" class="form-control mt-0" id="total_charges"
                                                name="total_charges" value="0" required readonly>
                                        </div>
                                        <div class="col-6">
                                            <label for="ticketing_date" class="form-label">Ticketing Date:</label>
                                            <input class="form-control mt-0" id="ticketing_date" name="ticketing_date"
                                                value="{{ date('Y-m-d') }}" required readonly>
                                        </div>
                                        <div class="col-6">
                                            <label for="ticketing_date" class="form-label">Total Selected Seats:</label>
                                            <input class="form-control mt-0" id="total_selected_seats"
                                                name="total_selected_seats" value="0" required readonly>
                                        </div>
                                        <div class="col-6">
                                            <button type="button" class="btn btn-danger rounded-pill  mt-2 mb-0 px-5" id="resetCustomerDetails">Reset</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button id="printTicketBtn" type="submit"
                                            class="col-12 btn btn-success rounded-pill px-5">Print
                                            Ticket</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>

        </div>
    </section>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
        integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
    <script>
        $(document).ready(function() {
            $('#train').change(function() {
                var trainId = $(this).val();
                $('#trainDate').empty();
                $('#trainDate').prop('disabled', false);

                $.ajax({
                    url: '{{ route('get.train.dates', ['trainId' => ':trainId']) }}'.replace(
                        ':trainId', trainId),
                    type: 'GET',
                    success: function(response) {
                        // console.log(response.data)
                        var dates = response.dates;
                        $('#trainDate').append('<option value="">Select Date</option>');
                        Object.keys(dates).forEach(function(key) {
                            $('#trainDate').append('<option value="' + key + '">' +
                                dates[key] + '</option>');
                        });
                    },
                    error: function(error) {
                        // console.log(error);
                    }
                });
            });
            $('#class').change(function() {
                console.log($('#cnic').val());
                var selectedClassId = $(this).val();
                var trainId = $('#train').val();
                $('#coach').empty();
                $('#coach').prop('disabled', false);

                $.ajax({
                    url: "{{ url('get-coaches-by-class') }}" + "/" + selectedClassId,
                    type: 'GET',
                    data: {
                        'train_id': trainId
                    },
                    success: function(response) {
                        var coaches = response.coaches;
                        // console.log(response);
                        $('#coach').empty();
                        $('#coach').append('<option value="">Select Coach</option>');
                        coaches.forEach(function(coach) {
                            $('#coach').append('<option value="' + coach.id + '">' +
                                coach.coach_name_or_no + '</option>');
                        });
                    },
                    error: function(error) {
                        // console.log(error);
                    }
                });
            });

            $('#ticketForm').submit(function(e) {
                e.preventDefault();
                var formData = $(this).serialize();

                $.ajax({
                    url: '{{ url('save-ticket') }}',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    success: function(response) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'Ticket saved successfully!',
                        });
                        // // console.log(response);
                        generatePrintPage(response.latest_index);
                        // window.location.reload();
                        fetchtrainorientation();
                        resetCustomerDetailsOnly();
                    },
                    error: function(xhr, status, error) {
                        // // console.log(xhr)
                        var errorMessage = xhr.responseText;
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: errorMessage,
                        });
                    }
                });
            });

            $('#seating_plan').click(function(e) {
                var train = $('#train option:selected').val();
                var coach_id = $('#coach option:selected').val();
                var seating_plan_coach_no = $('#coach option:selected').text();
                var seating_plan_class_name = $('#class option:selected').text();
                var class_id = $('#class option:selected').val();
                // var class_id = $('#class option:selected').val();
                var train_date = $('#trainDate option:selected').val();
                
                $('#seating_plan_coach_no').text(seating_plan_coach_no);
                
                if ($('#send_ajax_seat_plan').val() == 'send') {
                    $('#seating_plan_seats').empty();
                    // $('.loader-body').show();
                    if(seating_plan_class_name != 'AC Parlour'){
                        $('#seating_plan_seats').removeClass('row-cols-5');
                        $('#seating_plan_seats').addClass('row-cols-6');
                    }else{
                        $('#seating_plan_seats').removeClass('row-cols-6');
                        $('#seating_plan_seats').addClass('row-cols-5');
                    }
                var ajax_1 = $.ajax({
                    url: '{{ route('seating_plan') }}',
                    type: 'GET',
                    data: {
                        train: train,
                        coach_id: coach_id,
                        class_id: class_id,
                        train_date: train_date
                    },
                    success: function(response) {
                        var seats = response.seating_plan; 
                        var selectedSeats = [];
                    
                        response.seating_plan.forEach(function(item) {
                            var seatNumber = item.seat_number;
                            var seatId = item.id;
                            var selected_seats_already = $('#selected_seats').val();
                            var cardHtml = '';
                        
                            if (item.seating_person_gender != null) {
                                var genderImage = '';
                                if (item.seating_person_gender == 'Male') {
                                    genderImage = "{{ asset('app-assets/images/elements/worker 1.png') }}";
                                } else if (item.seating_person_gender == 'Female') {
                                    genderImage = "{{ asset('app-assets/images/avatars/woman-avatar 1.png') }}";
                                } else if (item.seating_person_gender == 'Family') {
                                    genderImage = "{{ asset('app-assets/images/elements/family (1) 1.png') }}";
                                }
                                cardHtml = `
                                    <div class="bg-danger rounded-3 text-center" id="${item.id}">
                                        <div class="text-start text-white">
                                            <small>&nbsp;${item.seat_number}</small>
                                        </div>
                                        <img src="${genderImage}" class="img-fluid rounded-start mx-2 mb-1" alt="...">
                                        <p class="m-0 text-white">${item.seating_person_gender}</p>
                                    </div>`;
                            } else {
                                var availableImage = "{{ asset('app-assets/images/avatars/sofa 8.png') }}";
                                cardHtml = `
                                    <div class="bg-white rounded-3 text-center" id="${item.id}">
                                        <div class="text-start text-success">
                                            <small>&nbsp;${item.seat_number}</small>
                                        </div>
                                        <img src="${availableImage}" class="img-fluid rounded-start mx-2 mb-1" alt="...">
                                        <p class="m-0 text-success">Available</p>
                                    </div>`;
                            }
                        
                            $('#seating_plan_seats').append(`<div class="card p-0 seating_plan_seat">${cardHtml}</div>`);
                        
                            if (item.seating_person_gender == null) {
                                if (!$('#' + seatId).data('click-bound')) {
                                    $('#' + seatId).data('click-bound', true);
                                    $('#' + seatId).click(function() {
                                        var seatIndex = selectedSeats.findIndex(function(seat) {
                                            return seat.id === item.id;
                                        });
                                    
                                        if (seatIndex !== -1) {
                                            var singleCharges = parseFloat($('#singleCharges').val());
                                            var totalCharges = parseFloat($('#total_charges').val());
                                            var totalChargesFinal = totalCharges - singleCharges;
                                            $('#total_charges').val(totalChargesFinal.toFixed(2));
                                            var current_selected_seats = parseInt($('#total_selected_seats').val());
                                            var new_selected_seats = current_selected_seats - 1;
                                            $('#total_selected_seats').val(new_selected_seats);

                                            selectedSeats.splice(seatIndex, 1);
                                            $('#' + seatId).removeClass('bg-success').addClass('bg-white');
                                            $('#' + seatId).find('div').removeClass('text-white').addClass('text-success');
                                            $('#' + seatId).find('p').removeClass('text-white').addClass('text-success');
                                        } else {
                                            var singleCharges = parseFloat($('#singleCharges').val());
                                            var totalCharges = parseFloat($('#total_charges').val());
                                            var totalChargesFinal = singleCharges + totalCharges;
                                            $('#total_charges').val(totalChargesFinal.toFixed(2));
                                            var current_selected_seats = parseInt($('#total_selected_seats').val());
                                            var new_selected_seats = current_selected_seats + 1;
                                            $('#total_selected_seats').val(new_selected_seats);
                                        
                                            var seatInfo = {
                                                id: item.id,
                                                name: $('#' + seatId).find('div').text().trim()
                                            };
                                            selectedSeats.push(seatInfo);
                                            $('#' + seatId).addClass('bg-success').removeClass('bg-white');
                                            $('#' + seatId).find('div').addClass('text-white').removeClass('text-success');
                                            $('#' + seatId).find('p').addClass('text-white').removeClass('text-success');
                                        }
                                    
                                        $('#selected_seats').val(JSON.stringify(selectedSeats));
                                    });
                                }
                            }
                        });
                        setTimeout(function() {
                            $('.loader-body').hide();
                        }, 800);
                    },
                    error: function(error) {
                        alert(error);
                        $('.loader-body').hide();
                    }
                });
            }
        });


            $('.reject_ajax_seat_plan').click(function(e) {
                $('#send_ajax_seat_plan').val('reject');
            });

            var ticketCounter = localStorage.getItem('ticketCounter') ? parseInt(localStorage.getItem(
                'ticketCounter')) : 1;
            $('#type').on('change', function() {
                // // console.log('lkjnsjfdb');
                var coachId = $('#coach').val();
                var type = $('#type').val();
                $('#seating_plan').prop('disabled', false);

                $.ajax({
                    url: '{{ url('get-type-charges') }}',
                    type: 'GET',
                    data: {
                        coachId: coachId,
                        type: type
                    },
                    success: function(response) {
                        $('#singleCharges').val(response.singleCharges)
                    },
                    error: function(error) {
                        // console.log(error);
                    }
                })
            });

            $('#coach').on('change', function() {
                // // console.log('lkjnsjfdb');
                var coachId = $('#coach').val();
                $('#charges_child_adult').removeClass('d-none');
                $('#seating_plan').prop('disabled', true);
                $('#type').prop('disabled', false);
                $('#send_ajax_seat_plan').val('send');
                $('#total_charges').val('0');
                $('#total_selected_seats').val('0');
                $('#type option:selected').prop('selected', false);

                $.ajax({
                    url: '{{ url('get-charges') }}',
                    type: 'GET',
                    data: {
                        coachId: coachId,
                    },
                    success: function(response) {
                        $('#child_charges').text(response.childCharges)
                        $('#adult_charges').text(response.adultCharges)
                    },
                    error: function(error) {
                        // console.log(error);
                    }
                })
            });

            // function calculateCharges() {
            //     // var seats = selected_seats;
            //     // alert('bhjvhj' + seats);
            //     // // console.log(seats + 'ljscbkhbk');
            //     var coachId = $('#coach').val();
            //     var type = $('#type').val();

            //     // $.ajax({
            //     //     url: '{{ url('get-type-charges') }}' + '/' + coachId,
            //     //     type: 'GET',
            //     //     data: {
            //     //         // seats: seats,
            //     //         type: type
            //     //     },
            //     //     success: function(response) {
            //     //         var typeCharges = response.typeCharges;

            //     //         $('#singleCharges').val(response.typeCharges);
            //     //         $('#charges').val(typeCharges.toFixed(2));
            //     //     },
            //     //     error: function(error) {
            //     //         // console.log(error);
            //     //     }
            //     // });

            // }

            $("#cnic:input").inputmask();

            function generatePrintPage(latest_index) {
                var printWindow = window.open('', '_blank');
                var train = $('#train option:selected').text();
                var train_id = $('#train').val();
                var name = $('#name').val();
                var cnic = $('#cnic').val();
                var addressPhone = $('#addressPhone').val();
                var className = $('#class option:selected').text();
                var coachName = $('#coach option:selected').text();
                var total_charges = $('#singleCharges').val();
                var trainDate = $('#trainDate option:selected').text();
                var selected_seats = $('#selected_seats').val();
                console.log(selected_seats);
                // var selectedSeats = '[{"id":2},{"id":5},{"id":6}]'; // Assuming this is your JSON string

                // Parse the JSON string into an array of objects
                var seatObjects = JSON.parse(selected_seats);
                console.log(seatObjects[1]);

                // Extract just the IDs from the array of objects
                var ids = seatObjects.map(function(seat) {
                    return seat.id;
                    console.log(seat);
                });

                // Calculate the length of the resulting array of IDs
                var length = ids.length;

                var seats = length;
                var latest_index;
                for (var i = 0; i < seats; i++) {
                    
                    var htmlContent = `
                <!DOCTYPE html>
                <html lang="en">

                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>TMS</title>
                    <style>
                        .text-center {
                            text-align: center !important;
                        }

                        .fw-bold {
                            font-weight: 900;
                        }

                        .m-0 {
                            margin: 0;
                        }
                    </style>
                </head>

                <body class="m-0" style="padding: 0px">
    <p class="fw-bold m-0" style="margin-top: 20px; margin-bottom: 0;"><samll>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ticket No: ${train_id}_${latest_index}</small></p>
    <p class="fw-bold m-0" style="margin-top: 13px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>Seat No: ${seatObjects[i].name}</small></p>
    <p class="fw-bold m-0" style="margin-top: 13px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>${name}</small></p>
    <p class="fw-bold m-0" style="margin-top: 12px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>${cnic}</small>
    </p>
    <p class="fw-bold m-0" style="margin-top: 11px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>${addressPhone}</small>
    </p>
    <p class="fw-bold m-0" style="margin-top: 17px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>${className}</small></p>
    <p class="fw-bold m-0" style="margin-top: 13px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>${coachName}</small></p>
    <p class="fw-bold m-0" style="margin-top: 13px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>RS
        ${total_charges}/-</small></p>
    <p class="fw-bold m-0" style="margin-top: 12px; margin-bottom: 0;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>${trainDate}</small>
    </p>
                </body>

                </html>`;
                    latest_index++;
                    // console.log(latest_index);
                    printWindow.document.write(htmlContent);
                    printWindow.document.write('<p style="page-break-after: always;"></p>');
                    ticketCounter++;
                }
                localStorage.setItem('ticketCounter', ticketCounter);
                printWindow.document.close();
                printWindow.print();
            }

            $('#resetCustomerDetails').on('click', resetCustomerDetailsOnly);
            $('#trainDate').on('change',fetchtrainorientation);
            function resetCustomerDetailsOnly() {
                $('#name').val('');
                $('#cnic').val('');
                $('#addressPhone').val('');
                $('#class').val(null);
                $('#coach').val(null);
                $('#type').val(null);
                $('#child_charges').text('');
                $('#adult_charges').text('');
                $('#total_charges').val('0');
                $('#total_selected_seats').val('0');
                $('#selected_seats').val('');
                $('#seating_plan_seats').empty();
            };
            function fetchtrainorientation(){
     var date = $('#trainDate').val();
                $('#class').empty();
                $('#class').prop('disabled', false);

                $.ajax({
                    url: '{{ route('get.seat.adjustments', ['date' => ':date']) }}'.replace(
                        ':date', date),
                    type: 'GET',
                    success: function(response) {

                        var seatAdjustments = response.seatAdjustments;
                        // console.log(response);

                        if (response.seatAdjustments == 0) {
                            $('#class').empty();
                            $('#coach').empty();
                            $('.seat-cards-container').empty();
                            var cardHtml = `
                <div class="col-12 col-sm-12">
                    <div class="card" style="background:#E3EBEE">
                        <div class="card-body">
                            <h5 class="card-title d-flex justify-content-center">No Schedule Found For That Date</h5>
                        </div>
                    </div>
                </div>
                `;
                            $('.seat-cards-container').append(cardHtml);
                        } else {
                            $('#class').empty();
                            $('#coach').empty();
                            $('.seat-cards-container').empty();

                            $('#class').append('<option value="">Select Class</option>');
                            $('#coach').append('<option value="">Select Coach</option>');
                            seatAdjustments.forEach(function(seatAdjustment) {
                                $('#class').append('<option value="' + seatAdjustment
                                    .train_class_id + '">' + seatAdjustment
                                    .train_class_name + '</option>');

                                seatAdjustment.coaches.forEach(function(coach) {
                                    $('#coach').append('<option value="' +
                                        coach + '">' + coach + '</option>');
                                });

                                var totalSeatsSum = seatAdjustment.total_seats.reduce((
                                    acc, val) => acc + val, 0);

                                var coachesHtml = seatAdjustment.coaches.map((coach,
                                    index) => `
                    <div class="d-flex justify-content-between">
                        <p class="card-text">${coach}:</p>
                        <p>${seatAdjustment.allocated_seats[index]} / ${seatAdjustment.total_seats[index]}</p>
                    </div>
                    `).join('');

                                var colors = ['#E3EBEE', '#D3E0DC', '#C3D7C2'];
                                var randomColor = colors[Math.floor(Math.random() *
                                    colors.length)];

                                var cardHtml = `
                    <div class="col-12 col-sm-6">
                        <div class="card" style="background:${randomColor}; opacity:0.8">
                            <div class="card-body">
                                <h5 class="card-title"><img src="game-icons_saloon.png" alt=""> ${seatAdjustment.train_class_name}</h5>
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">Coach Name</p>
                                    <p>Allocated/Total</p>
                                </div>
                                ${coachesHtml}
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">Total Seats:</p>
                                    <p>${totalSeatsSum}</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">Ticket Booked</p>
                                    <p>${seatAdjustment.allocated_seats.reduce((acc, val) => acc + val, 0)}</p>
                                </div>
                                <div class="d-flex justify-content-between">
                                    <p class="card-text">Available Ticket</p>
                                    <p>${totalSeatsSum - seatAdjustment.allocated_seats.reduce((acc, val) => acc + val, 0)}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;

                                $('.seat-cards-container').append(cardHtml);
                            });
                        }
                    },
                    error: function(error) {
                        // console.log(error);
                    }
                });
            }

        });
    </script>
@endsection
