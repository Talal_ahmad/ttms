@extends('admin.layouts.master')
@section('title', 'Schedule')

@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Schedule</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item"><a href="#">Schedule</a>
                                </li>
                                <li class="breadcrumb-item active">Schedule
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section>
                <div class="row">
                    <div class="col-12">
                        <div class="card pb-2">
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th class="not_include"></th>
                                        <th>Sr.No</th>
                                        <th>Schedule Day</th>
                                        <th>Train</th>
                                        <th class="not_include">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>

            <!--Add Modal -->
            <div class="modal fade text-start" id="add_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Add Schedule</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form class="form" id="add_form">
                            @csrf
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="schedule_day">Schedule Day</label>
                                            <input type="date" class="form-control" placeholder="schedule_day"
                                                name="schedule_day" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="train_id">Train</label>
                                            <select name="train_id" id="train_id" class="select2 form-select"
                                                data-placeholder="Select Train">
                                                @foreach ($trains as $train)
                                                    <option
                                                        value="{{ $train->id }}"{{ $train->id == request('train_id') ? 'selected' : '' }}>
                                                        {{ $train->title }}</option>
                                                @endforeach
                                            </select>
                                            {{-- <input type="text" class="form-control" placeholder="train_id"
                                                name="train_id" required /> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="form_save btn btn-primary" id="save">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End Add Modal -->

            <!--start edit Modal -->
            <div class="modal fade text-start" id="edit_modal" tabindex="-1" aria-labelledby="myModalLabel17"
                aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel17">Edit Schedule</h4>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <form class="form" id="edit_form">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_schedule_day">Schedule Day</label>
                                            <input type="date" class="form-control" id="edit_schedule_day"
                                                placeholder="Schedule Day" name="schedule_day" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <label class="form-label" for="edit_train_id">Train id</label>
                                            <select name="train_id" id="edit_train_id" class="select2 form-select"
                                                data-placeholder="Select Train">
                                                @foreach ($trains as $train)
                                                    <option
                                                        value="{{ $train->id }}"{{ $train->id == request('train_id') ? 'selected' : '' }}>
                                                        {{ $train->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-outline-success">Reset</button>
                                <button type="submit" class="form_save btn btn-primary" id="update">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--End edit Modal -->
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        var rowid;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: "{{ route('train_schedule.index') }}",
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: null,
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        data: 'schedule_day',
                        name: 'train_schedule.schedule_day'
                    },
                    {
                        data: 'train',
                        name: 'train.title'
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        searchable: false,
                        orderable: false,
                        render: function(data, type, full, meta) {
                            return (
                                '<a href="javascript:;" class="item-edit" onclick=edit(' + full
                                .id + ')>' +
                                feather.icons['edit'].toSvg({
                                    class: 'font-medium-4'
                                }) +
                                '</a>' +
                                '<a href="javascript:;" onclick="delete_item(' + full.id +
                                ')">' +
                                feather.icons['trash-2'].toSvg({
                                    class: 'font-medium-4 text-danger'
                                }) +
                                '</a>'
                            );
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                buttons: [
                    // {
                    // extend: 'collection',
                    // className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    // text: feather.icons['share'].toSvg({
                    //     class: 'font-small-4 me-50'
                    // }) + 'Export',
                    // buttons: [
                    //     {
                    //         extend: 'print',
                    //         text: feather.icons['printer'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Print',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'csv',
                    //         text: feather.icons['file-text'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Csv',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'excel',
                    //         text: feather.icons['file'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Excel',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'pdf',
                    //         text: feather.icons['clipboard'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Pdf',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     },
                    //     {
                    //         extend: 'copy',
                    //         text: feather.icons['copy'].toSvg({
                    //             class: 'font-small-4 me-50'
                    //         }) + 'Copy',
                    //         className: 'dropdown-item',
                    //         action: newexportaction,
                    //         exportOptions: {
                    //             columns: ':not(.not_include)'
                    //         }
                    //     }
                    // ],
                    // init: function(api, node, config) {
                    //     $(node).removeClass('btn-secondary');
                    //     $(node).parent().removeClass('btn-group');
                    //     setTimeout(function() {
                    //         $(node).closest('.dt-buttons').removeClass('btn-group')
                    //             .addClass('d-inline-flex');
                    //     }, 50);
                    // }
                    // },
                    {
                        text: feather.icons['plus'].toSvg({
                            class: 'me-50 font-small-4'
                        }) + 'Add New',
                        className: 'create-new btn btn-primary',
                        attr: {
                            'data-bs-toggle': 'modal',
                            'data-bs-target': '#add_modal'
                        },
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                        }
                    }
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Schedule</h6>');

            // add record
            $("#add_form").submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('train_schedule.store') }}",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    responsive: true,
                    success: function(response) {
                        $.unblockUI();
                        console.log(response);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            $("#add_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Schedule has been Added Successfully!'
                            })
                        }

                    }
                });
            });

            // Update record
            $("#edit_form").on("submit", function(e) {
                // alert('kakhsv');
                e.preventDefault();
                $.ajax({
                    url: "{{ url('train_schedule') }}" + "/" + rowid,
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        $.unblockUI();
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#edit_modal").modal("hide");
                            dataTable.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Schedule has been Updated Successfully!'
                            })
                        }

                    }
                });
            });
        });

        function edit(id) {
            rowid = id;
            $.ajax({
                url: "{{ url('train_schedule') }}" + "/" + id + "/edit",
                type: "get",
                success: function(response) {
                    $("#edit_schedule_day").val(response.schedule_day);
                    $("#edit_train_id").val(response.train_id);
                    $("#edit_modal").modal("show");
                },
            });
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "train_schedule/" + id,
                                type: "DELETE",
                                data: {
                                    _token: "{{ csrf_token() }}"
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Schedule has been Deleted Successfully!'
                                        })
                                    }
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
