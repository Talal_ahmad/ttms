@extends('admin.layouts.master')

@section('title', 'Dashboard')
<style>
    .card_grad_1 {
        background-image: linear-gradient(to right, #8e2de2, #4a00e0);
    }

    .card_grad_2 {
        background-image: linear-gradient(to right, #a8c0ff, #3f2b96);
    }

    .card_grad_3 {
        background-image: linear-gradient(to right, #2E3192, #1BFFFF);
    }

    .card_grad_4 {
        background-image: linear-gradient(to right, #0f0c29, #302b63, #24243e);
    }

    .unactive-link {
        color: #d0d2d6 !important
    }

    .dark-layout h5,
    .dark-layout h2 {
        color: white !important;
    }

    h2 {
        color: #000 !important;
    }

    .dark-layout body .card-text {
        color: white !important;
    }

    .booking_cards-card-text {
        color: #fff !important;
    }

    .booking_cards-card-title {
        color: #fff !important;
    }

    .custom-schedule-card-position {
        top: 5px !important;
        left: 9px !important;
    }

    .booking_cards:hover {
        box-shadow: 10px 7px 3px 3px grey !important;
        /* padding: 10px !important; */
        transform: translate(0px, -10px) !important;
        transition: all 0.4s ease-in-out 0s !important;
    }
</style>
@section('content')
    <section class="content-wrapper container-xxl p-0">
        {{-- <div class="content-header row"></div> --}}
        <div class="content-body">
            <h2>
                <a class=" text-decoration-none unactive-link " href="{{ asset('/') }}">Trains</a>
                @if (request()->is($train_id . '/train_schedule'))
                    /<a class=" text-decoration-none active " href="{{ asset($train_id . '/train_schedule') }}">
                        Train Schedules</a>
                @endif
                @if (request()->is($train_id . '/' . $train_schedule_id . '/train_booking'))
                    /<a class=" text-decoration-none unactive-link " href="{{ asset($train_id . '/train_schedule') }}">
                        Train Schedules</a>
                    /<a class=" text-decoration-none active "
                        href="{{ asset($train_id . '/' . $train_schedule_id . '/train_booking') }}">
                        Booking Details</a>
                @endif
            </h2>

            {{-- Train Cards --}}
            @if (request()->is('/'))
                <div class="row row-cols-lg-4 row-cols-md-3 m-1 mt-2">
                    @foreach ($trains as $train)
                        {{-- @dd($train) --}}
                        <div>
                            <div class="card">
                                <a href="{{ asset($train->id . '/train_schedule') }}">
                                    <div class="card-body">
                                        <h5 class="card-title fs-1">{{ $train->title }}</h5>
                                        <p class="card-text fs-4"><span class="fw-bold text-secondary ">Root: </span>
                                            {{ $train->root }}
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            {{-- Train Schedule Cards --}}
            @if (request()->is($train_id . '/train_schedule'))
                <div class="row row-cols-lg-4 row-cols-md-3 m-1 mt-2">
                    @foreach ($train_schedules as $key => $train_schedule)
                        {{-- @dd($train) --}}
                        <div>
                            <div class="card pb-1">
                                <a href="{{ asset($train_id . '/' . $train_schedule->id . '/train_booking') }}">
                                    <div class="card-body position-relative px-md-0">
                                        <span class="position-absolute custom-schedule-card-position">Schedule
                                            #{{ $key + 1 }}</span>
                                        <h5 class="card-title fs-1 mb-1 mt-2 text-center ">
                                            {{ \Carbon\Carbon::parse($train_schedule->schedule_day)->format('d-F-Y') }}</h5>
                                    </div>
                                </a>
                                {{-- @dd($train_schedule->schedule_day) --}}
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif

            {{-- Train Booking Cards --}}
            @if (request()->is($train_id . '/' . $train_schedule_id . '/train_booking'))
                {{-- Train Schedule Booking Detail Cards  --}}
                <div class="row row-cols-lg-4 row-cols-md-3 m-1 mt-2">
                    <div>
                        <div class="card booking_cards card_grad_1">
                            <div class="card-body px-1">
                                <h5 class="card-title booking_cards-card-title">Total Number of Seats: </h5>
                                <p class="card-text booking_cards-card-text fs-3">{{ $total_train_seats }}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card booking_cards card_grad_2">
                            <div class="card-body">
                                <h5 class="card-title booking_cards-card-title">Total Allocated Seats: </h5>
                                <p class="card-text booking_cards-card-text fs-3">
                                    {{ $total_train_schedule_allocated_seats }}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card booking_cards card_grad_3">
                            <div class="card-body">
                                <h5 class="card-title booking_cards-card-title">Total Remaining Seats: </h5>
                                <p class="card-text booking_cards-card-text fs-3">{{ $total_remaining_seats }}</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="card booking_cards card_grad_4">
                            <div class="card-body">
                                <h5 class="card-title booking_cards-card-title">Total Earning (Today): </h5>
                                <p class="card-text booking_cards-card-text fs-3">PKR {{ $train_bookings }}/-</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </section>
@endsection
@section('scripts')
    @if (request()->is('/'))
        <script>
            setTimeout(function() {
                toastr['success'](
                    'You have successfully logged in.',
                    '👋 Welcome {{ Auth::user()->name }}!', {
                        closeButton: true,
                        tapToDismiss: false
                    }
                );
            }, 2000);
        </script>
    @endif
@endsection
