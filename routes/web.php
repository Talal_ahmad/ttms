<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PusherNotificationController;
use App\Http\Controllers\POSController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\TrainController;
use App\Http\Controllers\ClassesController;
use App\Http\Controllers\CoachesController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\VendorGroupController;
use App\Http\Controllers\ScheduleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {

    Route::get('/', 'DashboardController@index');
    Route::get('{train_id}/train_schedule', 'DashboardController@index');
    Route::get('{train_id}/{train_schedule_id}/train_booking', 'DashboardController@index');
    Route::get('/pos', [POSController::class, 'pos'])->name('pos.index');
    Route::get('/seating_plan', [POSController::class, 'seating_plan'])->name('seating_plan');
    Route::get('/GenderOfSettingPersonINBooking', [POSController::class, 'GenderOfSettingPersonINBooking'])->name('GenderOfSettingPersonINBooking');
    Route::get('/get-train-dates/{trainId}', [POSController::class, 'getTrainDates'])->name('get.train.dates');
    Route::get('/get-seat-adjustments/{date}', [POSController::class, 'getSeatAdjustments'])->name('get.seat.adjustments');
    Route::get('/get-coaches-by-class/{classId}', [POSController::class, 'getCoachesByClass']);
    Route::get('get-type-charges', [POSController::class, 'getTypeCharges']);
    Route::get('get-charges', [POSController::class, 'getCharges']);
    Route::post('save-ticket', [BookingController::class, 'save'])->name('save-ticket');
    Route::get('booking_report',[ReportController::class,'booking_report']);
    
    Route::group(['middleware'=>'role_segregation'],function(){
        Route::get('profile', 'DashboardController@profile');
        Route::post('updateUserProfile', 'DashboardController@updateUserProfile');
        
        // Role
        Route::resource('roles', RoleController::class);
        // Permission
        Route::resource('permissions', PermissionController::class);
        // Users
        Route::resource('users', UserController::class);
    
        Route::get('/notification', function () {
            return view('admin.notifications.notification');
        });
        Route::resource('vendors_groups', VendorGroupController::class);
        Route::resource('vendors', VendorController::class);
        Route::resource('trains', TrainController::class);
        Route::resource('train_classes', ClassesController::class);
        Route::resource('train_coaches', CoachesController::class);
        Route::resource('train_schedule', ScheduleController::class);
    });

});
require __DIR__ . '/auth.php';
