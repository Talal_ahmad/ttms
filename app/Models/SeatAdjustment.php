<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeatAdjustment extends Model
{
    use HasFactory;
    protected $table = 'seat_adjustment';
    protected $fillable = [
        'train_id',
        'train_class_id',
        'train_coach_id',
        'train_schedule_id',
        'total_seats',
        'allocated_seats',
    ];
}
