<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coaches extends Model
{
    use HasFactory;
    protected $table = 'train_coaches';
    protected $guarded = [];
}
