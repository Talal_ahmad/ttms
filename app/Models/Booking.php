<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $table = 'booking';
    protected $fillable = [
        'train_schedule_id',
        'number_of_childs',
        'number_of_adults',
        'total_charges',
        'train_class_id',
        'train_coach_id',
        'customer_name',
        'customer_cnic',
        'customer_phone_number',
        'seat_number',
        'seat_id',
        'created_by',
    ];
}
