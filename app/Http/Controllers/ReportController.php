<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\User;
use App\Models\Train;
use App\Models\TrainSeat;
use App\Models\Schedule;
use App\Models\Coaches;
use App\Models\Classes;
use DB;

use Illuminate\Http\Request;


class ReportController extends Controller
{
  public function booking_report(Request $request)
  {
    $users = User::all();
    $trains = Train::all();
    $train_classes = Classes::all();
    $bookings = Booking::join('train_schedule', 'train_schedule.id', 'booking.train_schedule_id')
      ->join('trains', 'trains.id', 'train_schedule.train_id')
      ->join('train_classes', 'train_classes.id', 'booking.train_class_id')
      ->join('train_coaches', 'train_coaches.id', 'booking.train_coach_id')
      // ->join('train_seats', 'train_seats.id', 'booking.seat_id')
      ->join('users', 'users.id', 'booking.created_by');
    if ($request->fromdate) {
      $bookings->where('booking.created_at', '>=', $request->fromdate);
    }
    if ($request->todate) {
      $bookings->where('booking.created_at', '<=', $request->todate);
    }
    if (auth()->user()->is_vendor == 1) {
      $bookings->where('users.id', auth()->user()->id);
    } else {
      if($request->user){
        $bookings->whereIn('users.id', $request->user);
      }
    }
    if ($request->train) {
      $bookings->where('train_schedule.train_id', $request->train);
    }
    if ($request->train_schedule) {
      $bookings->where('train_schedule.id', $request->train_schedule);
    }
    if ($request->train_class) {
      $bookings->where('train_classes.id', $request->train_class);
    }
    if ($request->train_coach) {
      $bookings->where('train_coaches.id', $request->train_coach);
    }
    $bookings->select(
      'booking.total_charges',
      'customer_name',
      'customer_cnic',
      'customer_phone_number',
      'seating_person_gender',
      'schedule_day',
      'train_classes.title as class_name',
      'train_coaches.coach_name_or_no as coach_name',
      // 'train_seats.seat_number',
      'trains.title as train_name',
      \DB::raw("CONCAT(train_schedule.train_id, '_', booking.id) AS ticket_number"),
      \DB::raw("CONCAT(users.first_name, ' ', users.last_name) AS full_name")
    );

    $bookings = $bookings->get();
    if (auth()->user()->is_vendor == 1) {
      return view('admin.reports.booking_report_vendor', get_defined_vars());
    }
    return view('admin.reports.booking_report', get_defined_vars());
  }
}
