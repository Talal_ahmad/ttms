<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Schedule;
use App\Models\SeatAdjustment;
use Hash;
use Auth;
use App\Models\User;
use App\Models\Train;
use App\Models\TrainSeat;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // dd($request->all());
        if (Auth::user()->getRoleNames()[0] == 'TicketGenius') {
            $trains = Train::all(['id', 'title']);
            return view('admin.pos.index', get_defined_vars());
        }
        $trains = Train::get();
        $train_id = $request->train_id;
        $train_schedule_id = $request->train_schedule_id;
        $total_train_seats = TrainSeat::where('train_id', $train_id)->count();
        // dd($train_id);
        // dd($total_train_schedule_allocated_seats);

        if ($total_train_seats) {
            // dd($request->train_id);
            $train_schedules = Schedule::where('train_id', $train_id)->get();
            // dd($train_schedules);
        }
        if ($train_schedule_id) {
            // dd($request->train_schedule_id);
            $total_train_seats = SeatAdjustment::where('train_id', $train_id)
                ->where('train_schedule_id', $train_schedule_id)
                ->sum('total_seats');
            $total_train_schedule_allocated_seats = SeatAdjustment::where('train_id', $train_id)
                ->where('train_schedule_id', $train_schedule_id)
                ->sum('allocated_seats');
            $total_remaining_seats = $total_train_seats - $total_train_schedule_allocated_seats;
            $startTime = Carbon::now()->startOfDay();
            $endTime = Carbon::now()->endOfDay();
            $train_bookings = Booking::where('train_schedule_id', $train_schedule_id)
            ->whereBetween('created_at', [$startTime, $endTime])
            ->sum('total_charges');
            // dd($train_bookings);
        }
        return view('admin.index', get_defined_vars());
    }

    public function profile()
    {
        return view('admin.users.profile');
    }

    public function updateUserProfile(Request $request)
    {
        if (request()->query('type') == 'changePassowrd') {
            $this->validate($request, [
                'old_password' => 'required',
                'password' => 'required|min:8|max:20|confirmed',
            ]);
            $user = User::find(Auth::user()->id);
            if (Hash::check($request->old_password, $user->password)) {
                $user->fill([
                    'password' => Hash::make($request->password)
                ])->save();
                return redirect('profile?type=changePassowrd')->with('success_message', 'Password has been Updated Successfully');
            } else {
                return redirect('profile?type=changePassowrd')->with('error_message', 'Old Password didn\'t match');
            }
        } else {
            $user = User::find(Auth::user()->id);
            if ($request->hasFile('profile_pic')) {
                if (!empty($user->profile_photo_path)) {
                    unlink(public_path() . '/profile_pics/' . $user->profile_photo_path);
                }
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }
            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->email = $request->email;
            if (isset($profile_pic)) {
                $user->profile_photo_path = $profile_pic;
            }
            $user->update();
            return redirect('profile?type=general')->with('success_message', 'Profile has been Updated Successfully');
        }
    }
}
