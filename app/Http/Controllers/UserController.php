<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\User;
use App\Models\CompanyStructure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth'); 
    }

    public function index(Request $request)
    {
        if($request->ajax()){
            $query = User::leftjoin('model_has_roles' , 'model_has_roles.model_id' , '=' , 'users.id')->leftjoin('roles' , 'roles.id' , '=' , 'model_has_roles.role_id')->select('users.id', 'users.first_name','users.last_name', 'users.email', 'roles.name AS roleName', 'users.profile_photo_path');
            return DataTables::of($query)->make(true);
        }
        $roles = Role::get();
        return view('admin.users.index' , compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'first_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'last_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8|max:20|confirmed',
            ]);

            if($request->hasFile('profile_pic')){
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }

            $user = new User();
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->profile_photo_path = !empty($profile_pic) ? $profile_pic : NULL;
            $user->save();
            $user->assignRole($request->role);
            return ['code'=>'200', 'status'=>'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $role = DB::table('model_has_roles')->where('model_id' , $id)->select('role_id')->first();
        // dd($role);
        return response()->json([
            'user' => $user,
            'role' => $role,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->validate($request, [
                'first_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'last_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
            ]);

            $user = User::find($id);
            if($request->hasFile('profile_pic')){
                if(!empty($user->profile_photo_path)){
                    unlink(public_path().'/profile_pics/'.$user->profile_photo_path);
                }
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            if(isset($profile_pic)){
                $user->profile_photo_path = $profile_pic;
            }
            $user->update();
            $user->syncRoles($request->role);
            return ['code'=>'200', 'status'=>'success'];
        }catch(\Exception | ValidationException $e){
            if($e instanceof ValidationException){
                return ['code'=>'422','errors' => $e->errors()];
            }
            else{
                return ['code'=>'500','error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
