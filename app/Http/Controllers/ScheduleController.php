<?php

namespace App\Http\Controllers;

use App\Models\Coaches;
use App\Models\Train;
use App\Models\Classes;
use App\Models\Schedule;
use App\Models\SeatAdjustment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Schedule::leftjoin('trains', 'trains.id', '=', 'train_schedule.train_id')
                ->select('train_schedule.schedule_day as schedule_day', 'train_schedule.id as id', 'trains.title as train');
            return DataTables::of($query)->make(true);
        }

        $trains = Train::get(['id', 'title', 'root']);
        return view('admin.schedule.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'schedule_day' => 'required',
                'train_id' => 'required',
            ]);
            $data = $request->all();  
            $existingSchedule = Schedule::where('schedule_day', $data['schedule_day'])
                ->where('train_id', $data['train_id'])
                ->first();
            if ($existingSchedule) {
                return ['code' => '422', 'error_message' => 'A schedule already exists for this train on the selected day.'];
            }

            DB::transaction(function () use($request){
            $data = $request->all();  
            $data = Schedule::create($data);
            $seatdata = Train::join('train_coaches', 'train_coaches.train_id', '=', 'trains.id')
                ->join('train_classes', 'train_coaches.train_class_id', 'train_classes.id')
                ->select('trains.id as train_id', 'train_coaches.train_class_id', 'train_coaches.id as train_coach_id', 'train_coaches.number_of_seats')
                ->where('trains.id', $data->train_id)->get();
            foreach ($seatdata as $seat) {
                SeatAdjustment::create([
                    'train_id' => $seat->train_id,
                    'train_class_id' => $seat->train_class_id,
                    'train_coach_id' => $seat->train_coach_id,
                    'train_schedule_id' => $data->id,
                    'total_seats' => $seat->number_of_seats,
                    'allocated_seats' => 0,
                ]);
            }
            });
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Schedule::find($id);
        // dd($data);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'schedule_day' => 'required',
                'train_id' => 'required',
            ]);

            $data = $request->all();
            // dd($data);
            $updated = Schedule::find($id);
            $updated->fill($data)->update();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Schedule::find($id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
