<?php

namespace App\Http\Controllers;

use DataTables;

use App\Models\VendorGroup;
use Illuminate\Http\Request;

class VendorGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = VendorGroup::all();
            return DataTables::of($query)->make(true);
        }
        return view('admin.vendors_groups.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            $this->validate($request, [
                'title' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
            ]);

            $vendor_group = new VendorGroup();
            $vendor_group->title = $request->input('title');
            $vendor_group->save();
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VendorGroup  $vendorGroup
     * @return \Illuminate\Http\Response
     */
    public function show(VendorGroup $vendorGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VendorGroup  $vendorGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(VendorGroup $vendorGroup, $id)
    {
        $data = VendorGroup::find($id);
        // dd($data);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VendorGroup  $vendorGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VendorGroup $vendorGroup, $id)
    {
        try {
            $this->validate($request, [
                'title' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
            ]);

            $vendor_group = VendorGroup::find($id);
            $vendor_group->title = $request->input('title');
            $vendor_group->update();
            return ['code' => '200', 'status' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VendorGroup  $vendorGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(VendorGroup $vendorGroup, $id)
    {
        try {
            VendorGroup::find($id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
