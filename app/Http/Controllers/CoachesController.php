<?php

namespace App\Http\Controllers;

use App\Models\Coaches;
use App\Models\Train;
use App\Models\Classes;
use Illuminate\Http\Request;
use App\Models\TrainSeat;
use Yajra\DataTables\Facades\DataTables;

class CoachesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $query = Coaches::leftjoin('trains', 'trains.id', '=', 'train_coaches.train_id')
                ->leftjoin('train_classes', 'train_classes.id', '=', 'train_coaches.train_class_id')
                ->select('train_coaches.coach_name_or_no as coach_name_or_no', 'train_coaches.id as id', 'number_of_seats', 'trains.title as train', 'train_classes.title as train_classes', 'train_coaches.adult_charges', 'train_coaches.child_charges');
            return DataTables::of($query)->make(true);
        }

        $trains = Train::get(['id', 'title', 'root']);
        $classes = Classes::get(['id', 'title']);
        return view('admin.coaches.index', get_defined_vars());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'coach_name_or_no' => 'required',
                'number_of_seats' => 'required',
                'train_id' => 'required',
                'train_class_id' => 'required',
                'adult_charges' => 'required',
                'child_charges' => 'required',
            ]);
            $data = $request->except('train_class');
            // dd($data);
            $data = Coaches::create($data);
            $seat_number_ac_parlour = 1;
            $seat_number = 1;
            $row_index = 0;
            $characters = range('A', 'Z');
            // dd($request->train_class);

            while ($data->number_of_seats > 0) {
                $char = $characters[$row_index];
                // dd($char);
                $unique_seat_number = $char . '-' . $seat_number_ac_parlour;
                TrainSeat::create([
                    'seat_number' => $request->train_class == 'AC Parlour' ? $unique_seat_number : $seat_number,
                    'train_id' => $data->train_id,
                    'train_class_id' => $data->train_class_id,
                    'train_coaches_id' => $data->id
                ]);
                $data->number_of_seats--;
                $seat_number_ac_parlour++;
                $seat_number++;
                if ($seat_number_ac_parlour > 4) {
                    $row_index++;
                    $seat_number_ac_parlour = 1;
                }
            }
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Coaches::find($id);
        // dd($data);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'coach_name_or_no' => 'required',
                'number_of_seats' => 'required',
                'train_id' => 'required',
                'train_class_id' => 'required',
                'adult_charges' => 'required',
                'child_charges' => 'required',
            ]);

            $data = $request->except('train_class');
            $updated = Coaches::find($id);
            $updated->fill($data)->update();
            $seat_number = 1;
            $seat_number_ac_parlour = 1;
            $row_index = 0;
            $characters = range('A', 'Z');
          $seats=  TrainSeat::where('train_id', $data['train_id'])->where('train_class_id', $data['train_class_id'])->where('train_coaches_id', $updated->id)->get();
           foreach($seats as $seat) {
             $char = $characters[$row_index];
                $unique_seat_number = $char. '-' . $seat_number_ac_parlour;
                TrainSeat::where('id',$seat->id)->update([
                    'seat_number' => $request->train_class == 'AC Parlour' ? $unique_seat_number : $seat_number,
                    'train_id' => $data['train_id'],
                    'train_class_id' => $data['train_class_id'],
                    'train_coaches_id' => $updated->id
                ]);
                if ($seat_number % 4 == 0) {
                    $row_index++;
                    $seat_number_ac_parlour = 0;
                }
                $seat_number_ac_parlour++;
                $seat_number++;
            }
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception  | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Coaches::find($id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
