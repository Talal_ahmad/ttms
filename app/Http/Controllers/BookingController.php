<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use App\Models\Booking;
use App\Models\Schedule;
use App\Models\SeatAdjustment;
use App\Models\Vendor;


class BookingController extends Controller
{
    public function save(Request $request)
    {
       $request->validate([
            'train' => 'required',
            'trainDate' => 'required',
            'name' => 'required',
            'cnic' => 'required',
            'addressPhone' => 'required',
            'class' => 'required',
            'coach' => 'required',
            'type' => 'required',
            'charges' => 'required',
            'ticketing_date' => 'required',
        ]);
        $latest_index = Booking::latest()->pluck('id')->first();
        $selected_seats = json_decode($request->selected_seats[0], true);
        $latest_index ? $latest_index : 0;
        if (auth()->user()->is_vendor) {
            $allocated_seats = Booking::where([
                ['train_schedule_id', $request->trainDate],
                ['created_by', auth()->user()->id], ['train_coach_id', $request->coach],
                ['train_class_id', $request->class]
            ])->count();
            $total_allowed =  Vendor::where([
                ['user_id', auth()->user()->id], ['train_coaches_id', $request->coach],
                ['train_class_id', $request->class]
            ])->first();
            if (!empty($total_allowed)) {
                $total_allowed = $total_allowed->total_allowed_seats;
            } else {
                return response()->json( 'Your are not assigned to this coach', 404);
            }
            $remaing_seats =  $total_allowed -  $allocated_seats;
             if (count($selected_seats) > $remaing_seats ){
                 return response()->json('Your Seats Range has been Completed...!', 404);
        } 
    }
   ;
   $seatAdjustment = SeatAdjustment::where([
    ['train_id', Schedule::where('id', $request->trainDate)->first()->train_id],
    ['train_class_id', $request->class],
    ['train_coach_id', $request->coach],
    ['train_schedule_id', $request->trainDate],
])->first();

if ($seatAdjustment) {
    $seatAdjustment->update([
        'allocated_seats' =>$seatAdjustment->allocated_seats + count($selected_seats),
    ]); 
}
    foreach ($selected_seats as $seat_id) {
        Booking::create([
            'train_schedule_id' => $request->trainDate,
            'number_of_childs' => $request->type == 'child' ? $request->seats : 0,
            'number_of_adults' => $request->type == 'adult' ? $request->seats : 0,
            'seating_person_gender' => $request->seating_person_gender,
            'total_charges' => $request->charges,
            'train_class_id' => $request->class,
            'train_coach_id' => $request->coach,
            'customer_name' => $request->name,
            'customer_cnic' => $request->cnic,
            'customer_phone_number' => $request->addressPhone,
            'seat_id' => $seat_id['id'],
            'created_by' => auth()->user()->id,      
        ]);
    }

    // Return success response
    return response()->json(['success' => true, 'latest_index' => $latest_index]);
}
}
