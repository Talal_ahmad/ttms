<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Classes;
use Illuminate\Support\Facades\Hash;
use App\Models\Coaches;
use App\Models\Ticket;
use Spatie\Permission\Models\Role;
use App\Models\Train;
use App\Models\Vendor;
use App\Models\VendorGroup;
use Illuminate\Http\Request;
use DataTables;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(request $request)
    {
        $vendor_groups = VendorGroup::all();
        $tains = Train::all();
        $classes = Classes::all();
        $coaches = Coaches::all();
         $roles = Role::get();
        if ($request->ajax()) {
            $query = Vendor::join('users', 'ticket_vendors.user_id', '=', 'users.id')
                ->join('trains', 'ticket_vendors.train_id', '=', 'trains.id')
                ->join('train_classes', 'ticket_vendors.train_class_id', '=', 'train_classes.id')
                ->leftJoin('train_coaches', 'ticket_vendors.train_coaches_id', '=', 'train_coaches.id')
                ->select('ticket_vendors.*', 'users.first_name', 'trains.title', 'train_classes.title as train_classes_title', 'train_coaches.coach_name_or_no');
            return DataTables::of($query)->make(true);
        }
        // dd($user);

        return view('admin.vendors.index', compact('vendor_groups', 'tains', 'classes', 'coaches', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {
            // Validate the request data
            $request->validate([
                'first_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'last_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:8|max:20|confirmed',
                // 'total_allowed_seats' => 'required',

            ]);

            // Create the user
            $user = new User();
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->is_vendor = 1;
            // Handle profile picture upload if provided
            if ($request->hasFile('profile_pic')) {
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }
            $user->profile_photo_path = !empty($profile_pic) ? $profile_pic : NULL;
            $user->password = Hash::make($request->input('password'));
            $user->assignRole($request->role);
            $user->save();

            // Process the charges
            $charges = $request->input('charges');
            foreach ($charges as $charge) {
                $vendor = new Vendor();
                $vendor->vendor_name = $user->first_name . ' ' . $user->last_name;
                $vendor->user_id = $user->id;
                $vendor->train_id = $request->input('train_id');
                $vendor->train_class_id = $charge['train_class_id'];
                $vendor->train_coaches_id = $charge['train_coaches_id'];
                $vendor->total_allowed_seats = $charge['total_allowed_seats'];
                $vendor->vendor_group_id=$request->vendor_group_id;
                $vendor->save();
            }

            return response()->json(['code' => 200, 'status' => 'success']);
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return response()->json(['code' => 422, 'errors' => $e->errors()]);
            } else {
                return response()->json(['code' => 500, 'error_message' => $e->getMessage()]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit($vendor_id)
    {
        $query = Vendor::join('users', 'ticket_vendors.user_id', '=', 'users.id')
            ->join('trains', 'ticket_vendors.train_id', '=', 'trains.id')
            ->join('train_classes', 'ticket_vendors.train_class_id', '=', 'train_classes.id')
            ->leftJoin('train_coaches', 'ticket_vendors.train_coaches_id', '=', 'train_coaches.id')
            ->where('ticket_vendors.user_id', $vendor_id) // Use $vendor->id to access the ID of the resolved model instance
            ->select(
                'ticket_vendors.*',
                'users.id as user_id',
                'users.first_name',
                'users.last_name',
                'users.email',
                'trains.id as train_id',
                'train_classes.id as train_class_id',
                'train_coaches.id as train_coach_id',
                'train_coaches.coach_name_or_no'
            )
            ->get();
        $coaches = Coaches::all(); // Assuming TrainCoach is the model for coaches
        $classes = Classes::all(); // Assuming TrainClass is the model for classes
    
        return response()->json([
            'vendor' => $query,
            'coaches' => $coaches,
            'classes' => $classes
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request, [
                'first_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^',
                'last_name' => 'required|string|regex:^([a-zA-Z]+(.)?[\s]*)$^'
            ]);
    
            // Update user information
            $user = User::findOrFail($id);
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->save();
    
            $charges = $request->input('charges');
    
            foreach ($charges as $charge) {
                $newVendor = Vendor::where('train_id', $request->train_id)
                    ->where('train_class_id', $charge['train_class_id'])
                    ->where('train_coaches_id', $charge['train_coaches_id'])
                    ->where('user_id', $user->id)
                    ->first();
    
                if ($newVendor) {
                    $newVendor->update([
                        'vendor_name' => $user->first_name . ' ' . $user->last_name,
                        'train_id' => $request->input('train_id'),
                        'train_class_id' => $charge['train_class_id'],
                        'train_coaches_id' => $charge['train_coaches_id'],
                        'total_allowed_seats' => $charge['total_allowed_seats'],
                        'vendor_group_id'=>  $request->vendor_group_id,
                    ]);
                } 
            }
    
            return response()->json(['code' => 200, 'status' => 'success']);
        } catch (ValidationException $e) {
            return response()->json(['code' => 422, 'errors' => $e->errors()]);
        } catch (\Exception $e) {
            return response()->json(['code' => 500, 'error_message' => $e->getMessage()]);
        }
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        try {
            Vendor::find($vendor->id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
