<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;
use App\Models\Schedule;
use App\Models\SeatAdjustment;
use App\Models\Train;
use App\Models\Classes;
use App\Models\Coaches;
use App\Models\TrainSeat;
use App\Models\Vendor;
use Illuminate\Support\Facades\DB;

class POSController extends Controller
{
    public function pos()
    {
        $trains = Train::all(['id', 'title']);
        return view('admin.pos.index', compact('trains'));
    }

    public function getCoachesByClass(Request $request, $classId)
    {
        if (auth()->user()->is_vendor) {
            $coaches = Vendor::where('train_id', $request->train_id)
                ->where('train_class_id', $classId)
                ->where('user_id', auth()->user()->id)
                ->pluck('train_coaches_id')->toArray();
            $coaches = Coaches::where('train_class_id', $classId)
                ->whereIn('id', $coaches)
                ->get();
        } else {
            $coaches = Coaches::where('train_class_id', $classId)->get();
        }

        return response()->json([
            'coaches' => $coaches
        ]);
    }


    public function getTypeCharges(Request $request)
    {
        if ($request->type == 2) {
            $singleCharges = Coaches::find($request->coachId)->child_charges;
            return response()->json([
                'singleCharges' => $singleCharges
            ]);
        }
        if ($request->type == 1) {
            $singleCharges = Coaches::find($request->coachId)->adult_charges;
            return response()->json([
                'singleCharges' => $singleCharges
            ]);
        }
    }

    public function getCharges(Request $request)
    {
        $coaches = Coaches::find($request->coachId);
        $childCharges = $coaches->child_charges;
        $adultCharges = $coaches->adult_charges;

        return response()->json([
            'childCharges' => $childCharges,
            'adultCharges' => $adultCharges
        ]);
    }

    public function getTrainDates($trainId)
    {
        $dates = Schedule::where('train_id', $trainId)->pluck('schedule_day', 'id');

        if ($dates->isEmpty()) {
            return response()->json(['error' => 'Dates not found for the train'], 404);
        }

        // dd(response()->json(['dates' => $dates]));

        return response()->json(['dates' => $dates]);
    }

    public function getSeatAdjustments($date)
    {
        $trainSchedule = Schedule::where('id', $date)->first();

        if (!$trainSchedule) {
            return response()->json(['message' => 'No train schedule found for the given date'], 404);
        }

        $seatAdjustments = SeatAdjustment::where('train_schedule_id', $trainSchedule->id)
            ->join('train_coaches', 'seat_adjustment.train_coach_id', '=', 'train_coaches.id')
            ->join('train_classes', 'train_coaches.train_class_id', '=', 'train_classes.id')
            ->select(
                'train_classes.id as train_class_id',
                'train_classes.title as train_class_name',
                'train_coaches.coach_name_or_no',
                'seat_adjustment.total_seats',
                'seat_adjustment.allocated_seats'
            )
            ->get();

        // Group the seat adjustments by train_class_id
        $groupedSeatAdjustments = $seatAdjustments->groupBy('train_class_id')->map(function ($item) {
            return [
                'train_class_id' => $item->first()->train_class_id,
                'train_class_name' => $item->first()->train_class_name,
                'coaches' => $item->pluck('coach_name_or_no'),
                'total_seats' => $item->pluck('total_seats'),
                'allocated_seats' => $item->pluck('allocated_seats')
            ];
        });

        return response()->json(['seatAdjustments' => $groupedSeatAdjustments->values()]);
    }

    public function seating_plan(Request $request)
    {

        $seating_plan = TrainSeat::leftJoin('booking', function ($join) use ($request) {
            $join->on('booking.seat_id', '=', 'train_seats.id')
                ->where('booking.train_schedule_id', '=', $request->train_date);
        })
            ->where('train_seats.train_id', '=', $request->train)
            ->where('train_seats.train_coaches_id', '=', $request->coach_id)
            ->where('train_seats.train_class_id', '=', $request->class_id)
            ->orderBy('train_seats.id', 'asc')
            ->select(
                'train_seats.*',
                'booking.seating_person_gender'
            )
            ->get();
        return response()->json(['seating_plan' => $seating_plan]);
    }

    public function GenderOfSettingPersonINBooking(Request $request)
    {
        // dd('jsdbskjdb');
        // getGenderOfSettingPersonINBooking($request->train_date, $request->seat_id);

        return response()->json(['gender' => getGenderOfSettingPersonINBooking($request->train_date, $request->seat_id)]);
    }
}
