<?php

use App\Models\Booking;

if (!function_exists('getGenderOfSettingPersonINBooking')) {
    function getGenderOfSettingPersonINBooking($train_schedule_id, $seat_id)
    {
        $seating_person_gender = Booking::where('train_schedule_id', $train_schedule_id)
            ->where('seat_id', $seat_id)->first();
        if ($seating_person_gender) {
            return $seating_person_gender->seating_person_gender;
        } else {
            return 0;
        }
    }
}
